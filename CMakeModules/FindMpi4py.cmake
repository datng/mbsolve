# - Try to find mpi4py lib
#
# Once done this will define
#
#  MPI4PY_FOUND - system has mpi4py lib
#  MPI4PY_INCLUDE_DIR - the mpi4py include directory
#
# This module reads hints about search locations from
# the following enviroment variables:
#
# MPI4PY_ROOT
# MPI4PY_ROOT_DIR

# Copyright (c) 2006, 2007 Montel Laurent, <montel@kde.org>
# Copyright (c) 2008, 2009 Gael Guennebaud, <g.gael@free.fr>
# Copyright (c) 2009 Benoit Jacob <jacob.benoit.1@gmail.com>
# Redistribution and use is allowed according to the terms of the 2-clause BSD license.

if (MPI4PY_INCLUDE_DIR)

  # in cache already
  set(MPI4PY_FOUND TRUE)

else (MPI4PY_INCLUDE_DIR)

  if(NOT MPI4PY_INCLUDE_DIR)
    find_path(MPI4PY_INCLUDE_DIR NAMES mpi4py.i
        HINTS
        ENV MPI4PY_ROOT
        ENV MPI4PY_ROOT_DIR
        PATHS
        ${CMAKE_INSTALL_PREFIX}/include
        ${KDE4_INCLUDE_DIR}
        PATH_SUFFIXES mpi4py
      )
  endif(NOT MPI4PY_INCLUDE_DIR)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(Mpi4py DEFAULT_MSG MPI4PY_INCLUDE_DIR)

  mark_as_advanced(MPI4PY_INCLUDE_DIR)

endif(MPI4PY_INCLUDE_DIR)
