/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#ifndef MATRIX_EXP_ACTION_HANDLER_HPP
#define MATRIX_EXP_ACTION_HANDLER_HPP

#include <Eigen/Dense>
#include <Eigen/QR>
#include <Eigen/src/Core/NumTraits.h>
#include <vector>
#include <chrono>
#include <types.hpp>
#include <limits>
#include <unsupported/Eigen/MatrixFunctions>

namespace mbsolve {
/**
* The implemention of the work by Awad H. Al-Mohy and Nicholas J. Higham
* [1] "Computing the Action of the Matrix Exponential,
* with an Application to Exponential Integrators"
* Read More: https://epubs.siam.org/doi/abs/10.1137/100788860
*
* Calculates exp(mat*t)*b, where mat & b are matrices,
* and t is double.
*/
/**
* Values of theta for double precision.
* Added some more values, compared to the paper (1-30)
* 31-55: repeated values.
* */
static const real theta_m[55]{
  2.29e-16, 2.58e-8, 1.39e-5, 3.40e-4, 2.4e-3,
  9.07e-3, 2.38e-2, 5.00e-2, 8.96e-2, 1.44e-1,
  2.14e-1, 3.00e-1, 4.00e-1, 5.14e-1, 6.41e-1,
  7.81e-1, 9.31e-1, 1.09, 1.26, 1.4e0,
  1.62, 1.82, 2.01, 2.22, 2.43e0,
  2.64, 2.86, 3.08, 3.31, 3.54e0,
  3.54e0,3.54e0,3.54e0,4.7e0,4.7e0,
  4.7e0,4.7e0,4.7e0,6.0e0,6.0e0,
  6.0e0,6.0e0,6.0e0,7.2e0,7.2e0,
  7.2e0,7.2e0,7.2e0,8.5e0,8.5e0,
  8.5e0,8.5e0,8.5e0,9.9e0, 9.9e0};

/**
* 1/theta_m_double_precision:
* pre-calculated to speed up the parameter approximation
*/
static const real theta_m_quotient[55]{
  4.36e15, 3.88e7, 7.2e4, 2.9e3, 4.17e2,
  1.1e2, 4.2e1, 2e1, 1.12e1, 6.94,
  4.67, 3.33, 2.5, 1.95, 1.56,
  1.28, 1.07, 0.92, 0.79, 0.71,
  0.62, 0.55, 0.498, 0.45, 0.41,
  0.379, 0.35, 0.32, 0.30, 0.28,
  0.28,0.28,0.28,0.21,0.21,
  0.21,0.21,0.21,0.167,0.167,
  0.167,0.167,0.167,0.139,0.139,
  0.139,0.139,0.139,0.12,0.12,
  0.12,0.12,0.12,0.101,0.101};


/**
* Includes the Action on Matrix Exp Method.
* Using only dynamically allocated (Eigen::MatrixXd) matrices.
* Made for Reduntant Vector Representation where
* the density is a vector of size N^2xN^2.
*/
template<unsigned int num_lvl>
class matrix_exp_action_handler {

     static const unsigned int vec_len = num_lvl * num_lvl;
     typedef Eigen::MatrixXd real_matrix_t;
     typedef Eigen::MatrixXcd complex_matrix_t;
     typedef Eigen::Matrix<real, vec_len, 1> density_t;

    /**
    * p_max as proposed in the paper [1, p. 494]
    */
    static constexpr int p_max = 8;
    /**
    * m_max as proposed in the paper [1, p. 494]
    */
    static constexpr int m_max = 55;
    /**
    * The accuracy we want to achieve
    */
    static constexpr real tol = std::numeric_limits<real>::epsilon();

    /**
    * Eigen implementation of the L1 Norm.
    */
    static inline real l1norm(const real_matrix_t& m) {
        return m.colwise().template lpNorm<1>().maxCoeff();
    }

public:
    /**
    * Constructor
    */
    matrix_exp_action_handler() {}

    /**
    * Selects the Taylor degree.
    * Implemented as the Reference implementation in Matlab by Higham.
    * @param [in] A matrix A*t
    * @param [in] M zero matrix to fill
    */
    static inline void select_taylor_degree(Eigen::MatrixXd& M,
            const real_matrix_t& A, int& m, int& s)
    {
        /* mv: nr of matrix-vector multiplications */
        int mv = 0;
        real normA = l1norm(A); // exact norm
        real unA = 0.0;
        real c = 0.0;
        Eigen::Matrix<real, p_max-1, 1> alpha;
        alpha *= 0.;
        if (normA <= 4*theta_m[54]*p_max*(p_max+3)/m_max*1) {
            unA = 1;
            c = normA;
            for (int i=0; i<p_max-1; ++i){
                alpha(i) = c;
            }
        } else {
            unA = 1;
            c = normA;
            for (int i=0; i<p_max-1; ++i){
                alpha(i) = c;
            }
        }

        for (int p = 1; p < p_max; ++p) {
            for (int m = p*(p+1)-2; m < m_max; ++m) {
                M(m, p-1) = ceil(alpha(p-1) * theta_m_quotient[m]); // p: 0-6, m: 0-54 array indices
            }
        }

    }

    /**
    * Reference implementation (from Matlab) for finding the m&s parameters.
    * @param [in] mat Matrix A*t
    * @param [in] m
    * @param [in] s
    */
    static void set_approximation_parameter_v2(const real_matrix_t& mat,
                          int& m, int& s)
    {
        Eigen::MatrixXd M = Eigen::MatrixXd::Ones(m_max, p_max-1);
        M *= std::numeric_limits<real>::infinity();

        select_taylor_degree(M, mat, m, s);

        const Eigen::MatrixXd VecDiag = Eigen::Matrix<real ,1 ,55>::LinSpaced(1, 55);
        Eigen::MatrixXd C = Eigen::MatrixXd::Zero(p_max-1, m_max);
        /* multiplies col 1 with VecDiag(0), etc...*/
        C = M.transpose() * VecDiag.asDiagonal();
        int x{0}, y{0};

        /** NOTE
         * In select_taylor_degree() -> alpha is constant,
         * because normAm() not implemented!
         * Ergo: Find the minimum in the vector, not matrix
         * double cost = C.minCoeff();
         * */

        real cost = C.row(0).minCoeff(&x,&y);
        m = y;
        if (cost == std::numeric_limits<real>::infinity()) {
            cost = 0;
        }
        s = (cost/m > 1) ? cost/m : 1;
    }
    /**
    * Calculates exp(A*t)*b
    * Shift (Matlab) = true
    * @param [in] b density vector in rvr
    * @param [in] mat Superoperator in rvr
    * @param [in] t time step
    */
    static inline void action(real_matrix_t& mat, density_t& b,
                                                        const real& t = 1.0)
    {
        /* Shift the matrix*/
        real mu = mat.trace() / vec_len;
        for (int i = 0; i < vec_len; ++i) {
            mat(i, i) -= mu;
        }

        int m{0}, s{0};
        mat *= t;
        /* Test without set_approximation_param with m=9 */
        set_approximation_parameter_v2(mat, m, s);

        density_t B = density_t::Zero(vec_len,1);
        B = b;
        density_t F = density_t::Zero(vec_len,1);
        F = b;

        const real eta = std::exp(t * mu / s);
        real c1{0.}, c2{0.};
        for (int i = 1; i < s + 1; ++i) {
            c1 = B.template lpNorm<Eigen::Infinity>();
            if (m > 0) {
                for (int j = 1; j < m + 1; ++j) {
                    B = (1. / (s * j)) * (mat * B);
                    c2 = B.template lpNorm<Eigen::Infinity>();
                    F += B;
                    if (c1 + c2 <= tol * F.template lpNorm<Eigen::Infinity>()) {
                        /* Early termination */
                        break;
                    }
                    c1 = c2;
                }
            }
            F *= eta;
            B = F;
        }
        b = F;
    } // action()

};

}  // namespace mbsolve

#endif
