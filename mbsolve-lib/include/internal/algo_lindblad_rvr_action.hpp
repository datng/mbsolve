/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#ifndef MBSOLVE_ALGO_LINDBLAD_RVR_ACTION_H
#define MBSOLVE_ALGO_LINDBLAD_RVR_ACTION_H

#include <Eigen/Core>
#include <unsupported/Eigen/MatrixFunctions>
#include <qm_description.hpp>
#include <internal/redundant_vector_representation.hpp>
#include <internal/matrix_exp_action_handler.hpp>
#include <iostream>

namespace mbsolve {

/**
 * Solves the Lindblad equation using a redundant vector represenation
 * for the desity matrix, the AlMohy et. al. to calculate the matrix exponential.
 *
 * For details see literature:
 * Bidégaray, B. et al., Introducing physical relaxation terms in Bloch
 * equations, J. Comp. Phys., Vol. 170, Issue 2, 2001
 * https://doi.org/10.1006/jcph.2001.6752.
 *
 * TODO: add the Al Mohy Action on Matrix Exponential paper
 *
 * Use only internally to implement solvers.
 * \ingroup MBSOLVE_LIB_INT
 */

template<unsigned int num_lvl>
class lindblad_rvr_action
{
private:

public:
    static std::string name() { return "rvr-action"; }

    static const unsigned int vec_len = num_lvl * num_lvl;
    //typedef Eigen::Matrix<real, vec_len, vec_len> real_matrix_t;
    typedef Eigen::MatrixXd real_matrix_t;
    //typedef Eigen::Matrix<complex, vec_len, vec_len> complex_matrix_t;
    typedef Eigen::MatrixXcd complex_matrix_t;

    /* TODO: is there a Eigen Hermitian Matrix type? */
    typedef complex_matrix_t hermitian_matrix_t;

    /* dim - (N*N, 1) vector representation */
    typedef Eigen::Matrix<real, vec_len, 1> density;

    class sim_constants {
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        /* time step size */
        real dt;
        /* time-independent Hamiltonian operator. Dim: N*N x N*N  */
        real_matrix_t H0;
        /* dipole moment operator. Dim: N*N x N*N */
        real_matrix_t mu;
        /* dipole moment operator in vector form. Dim: N*N x 1 */
        density v;
        /* relaxation superoperator */
        real_matrix_t G;
        /* time-independent part of the Lindblad equation rhs */
        real_matrix_t M;
        /* carrier density */
        real carrier_dens;
        /* pre-calculated G+H0*/
        real_matrix_t GplusH0;
    };

    typedef Eigen::aligned_allocator<sim_constants> allocator;

    static inline hermitian_matrix_t
    convert_qm_operator(const qm_operator& op)
    {
        hermitian_matrix_t op_mat = hermitian_matrix_t::Zero(vec_len, vec_len);

        /* main diagonal elements */
        for (int i = 0; i < num_lvl; i++) {
            op_mat(i, i) = op.get_main_diagonal()[i];
        }

        /* off-diagonal elements */
        for (int i = 0, row = 0, col = 1; (row < num_lvl) && (col < num_lvl)
                 && (i < op.get_off_diagonal().size()); i++) {

            /* top half */
            op_mat(row, col) = op.get_off_diagonal()[i];

            /* bottom half */
            op_mat(col, row) = std::conj(op.get_off_diagonal()[i]);

            if (row == col - 1) {
                row = 0;
                col++;
            } else {
                row++;
            }
        }

        return op_mat;
    }

    static inline sim_constants
    get_qm_constants(std::shared_ptr<const qm_description> qm,
                     real time_step) {
        sim_constants sc;

        /* time step size */
        sc.dt = time_step;

        if (qm) {
        	rv_representation rvr(qm);
            /* Hamiltonian */
            sc.H0 = rvr.get_hamiltonian();
            /* dipole moment operator */
            sc.mu = rvr.get_dipole_operator();
            /* diplome moment operator in vector form */
            sc.v  = rvr.get_dipole_operator_vec();
            sc.G = rvr.get_relaxation_superop();
            /* time-independent Hamiltonian contribution in cvr */
            real_matrix_t M_0 = rvr.get_hamiltonian();
            /* time-independent part of the Lindblad equation rhs -> for the polarization */
            sc.M = M_0 + sc.G;
            /* pre-calculated GplusH0 */
            sc.GplusH0 = sc.G + sc.H0;
            /* carrier density */
            sc.carrier_dens = qm->get_carrier_density();
        } else {
            sc.H0 = real_matrix_t::Zero(vec_len, vec_len);
            sc.mu = real_matrix_t::Zero(vec_len, vec_len);
            sc.M = real_matrix_t::Zero(vec_len, vec_len);
            sc.G = real_matrix_t::Zero(vec_len, vec_len);
            sc.v = density::Zero(vec_len);
            sc.GplusH0 = real_matrix_t::Zero(vec_len, vec_len);
            sc.carrier_dens = 0.0;
        }

        return sc;
    }

public:
    static inline void
    update(const sim_constants& sc, density& d, real e, real *p_t) {
        /* determine complete Hamiltonian */
        real_matrix_t BigOp = sc.GplusH0 - e * sc.mu;
        matrix_exp_action_handler<num_lvl>::action(BigOp, d, sc.dt);

        /* update polarization term */
        *p_t = sc.carrier_dens * sc.v.transpose() * (sc.M * d);
    }

    static inline real
    calc_inversion(const density& d) {
        return d(vec_len - num_lvl + 1) - d(vec_len - num_lvl);
    }

    static inline real
    calc_population(const density& d, unsigned int idx) {
        return d(idx + vec_len - num_lvl);
    }

    static inline density
    get_density(const qm_operator& op) {
    	/* Initial density distribution -> used at setup !!! */
        complex_matrix_t rho = convert_qm_operator(op);
        density ret = density::Zero(vec_len);
        unsigned int pop_idx = vec_len - num_lvl;
        for (int i =0; i < num_lvl; ++i) {
            ret(pop_idx + i) = rho(i, i).real();
        }
        return ret;
    }

    static inline density
    get_density() {
        return density::Zero(vec_len);
    }

};

}

#endif
