/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#ifndef MBSOLVE_COHERENCE_REDUNDANT_VECTOR_REPRESENTATION_H
#define MBSOLVE_COHERENCE_REDUNDANT_VECTOR_REPRESENTATION_H

#include <Eigen/Dense>
#include <qm_description.hpp>

namespace mbsolve {

/**
 * Provides helper functions for the coherence vector representation.
 * \ingroup MBSOLVE_LIB_INT
 */
class rv_representation
{
private:
    const unsigned int m_num_levels;
    /* m_num_adj = m_num_levels * m_num_levels */
    const unsigned int m_num_adj;  

    typedef Eigen::Matrix<real, Eigen::Dynamic, Eigen::Dynamic> real_matrix_t;
    typedef Eigen::Matrix<complex, Eigen::Dynamic, Eigen::Dynamic>
    complex_matrix_t;

    real_matrix_t m_hamiltonian;
    real_matrix_t m_dipole_op;
    real_matrix_t m_dipole_op_vec;
    real_matrix_t m_relax_superop;

    std::vector<complex_matrix_t> m_generators;
    
    void
    setup_generators()
    {
        m_generators.clear();
        real c = 1/sqrt(2);
        /* set up SU(N) generators u -- real part off-diagonal elements */
        for (int k = 0; k < m_num_levels; k++) {
            for (int j = 0; j < k; j++) {
                complex_matrix_t g =
                    complex_matrix_t::Zero(m_num_levels, m_num_levels);
                g(j, k) = 1 * c;
                g(k, j) = 1 * c;
                m_generators.push_back(g);
            }
        }

        /* set up SU(N) generators v -- imag part off-diagonal elements */
        for (int k = 0; k < m_num_levels; k++) {
            for (int j = 0; j < k; j++) {
                complex_matrix_t g =
                    complex_matrix_t::Zero(m_num_levels, m_num_levels);
                g(j, k) = complex(0, -1 * c);
                g(k, j) = complex(0, +1 * c);
                m_generators.push_back(g);
            }
        }

        /* set up SU(N) generators w -- main-diagonal elements 
        *  n levels = n generators: redundancy */
        for (int l = 0; l < m_num_levels; l++) {
            complex_matrix_t g =
                complex_matrix_t::Zero(m_num_levels, m_num_levels);
            g(l, l) = 1;
            m_generators.push_back(g);
        }
    }

    /**
     * Transforms qm_operator to complex matrix.
     */
    complex_matrix_t
    convert_qm_operator(const qm_operator& op) const
    {
        complex_matrix_t op_mat =
            complex_matrix_t::Zero(m_num_levels, m_num_levels);

        for (int i = 0; i < m_num_levels; i++) {
            /* main diagonal elements */
            op_mat(i, i) = op.get_main_diagonal()[i];
        }

        for (int i = 0, row = 0, col = 1; i < op.get_off_diagonal().size();
             i++) {

            /* off-diagonal, top half */
            op_mat(row, col) = op.get_off_diagonal()[i];

            /* off-diagonal, bottom half */
            op_mat(col, row) = std::conj(op.get_off_diagonal()[i]);

            if (row == col - 1) {
                row = 0;
                col++;
            } else {
                row++;
            }
        }

        return op_mat;
    }

    qm_operator
    convert_qm_operator(complex_matrix_t mat) const
    /* NOTE convert matrix to into a qm_operator (on/off diag elements) */ 
    {
        /* main diagonal elements */
        std::vector<real> main_diag;
        for (int i = 0; i < m_num_levels; i++) {
            main_diag.push_back(mat(i, i).real());
        }

        /* TODO: hermiticity check? */
        /* off-diagonal elements */
        std::vector<complex> off_diag;
        for (int i = 1; i < m_num_levels; i++) {
            for (int j = 0; j < i; j++) {
                off_diag.push_back(mat(j, i));
            }
        }

        return qm_operator(main_diag, off_diag);
    }

    real_matrix_t
    calc_liouvillian(const qm_operator& op) const   
    {
        complex_matrix_t op_mat = convert_qm_operator(op);
        /* determine Liouvillian with respect to operator op 
         * NOTE what exactly does this mean? 
         * -> convert complex_matrix into a real_matrix
         * -> dimensions stay the same 
         * -> use the hermitian characteristic: save real/imag part separately
         * ---------
         * -> @cvr: liouvillian is of dim (N*N-1)x(N*N-1), rvr needs (N*N)x(N*N)
         * -> m_num_adj WAS (N*N-1); so 0:m_num_adj produces (N*N-1) steps
         * -> @rvr m_num_adj IS (N*N); so correct dimensions
        */
        real_matrix_t ret(m_num_adj, m_num_adj);
        for (int i = 0; i < m_num_adj; i++) {
            for (int j = 0; j < m_num_adj; j++) {
                complex_matrix_t result = op_mat *
                    (m_generators[i] * m_generators[j] -
                     m_generators[j] * m_generators[i]);
                complex c = complex(0, 1) * result.trace(); // * 0.5 faellt weg
                /* QUESTION should be real anyway? */
                ret(i, j) = c.real() * 1.0/HBAR;
            }
        }
        return ret;
    }

    real_matrix_t
    calc_op_vec(const qm_operator& op) const
    {
        complex_matrix_t op_mat = convert_qm_operator(op);
        real_matrix_t ret(m_num_adj, 1);

        for (int i = 0; i < m_num_adj; i++) {
            complex_matrix_t m = op_mat * m_generators[i];
            ret(i) = m.real().trace();
        }

        return ret;
    }

    real_matrix_t
    calc_relaxation_superop(std::shared_ptr<qm_superop> op) const
    {
        /* NOTE relaxation superop in the new basis */
        real_matrix_t ret = real_matrix_t::Zero(m_num_adj, m_num_adj);

        for (int i = 0; i < m_num_adj; i++) {
            for (int j = 0; j < m_num_adj; j++) {
                qm_operator a = convert_qm_operator(m_generators[j]);  // convert complex matrix to qm_operator
                /* take this new operator a and call a function -> action()
                 * -> overloaded () operator */
                qm_operator b = (*op)(a);  
                complex_matrix_t c = convert_qm_operator(b);
                complex_matrix_t d = c * m_generators[i];
                complex e = d.trace(); // * 0.5 
                ret(i, j) = e.real();
            }
        }
        return ret;
    }

    real_matrix_t
    calc_equi_vec(std::shared_ptr<qm_superop> op) const
    {
        real_matrix_t ret = real_matrix_t::Zero(m_num_adj, 1);
        complex_matrix_t a =
            complex_matrix_t::Identity(m_num_levels, m_num_levels);
        qm_operator b = convert_qm_operator(a);
        qm_operator c = (*op)(b);
        complex_matrix_t d = convert_qm_operator(c);

        for (int i = 0; i < m_num_adj; i++) {
            complex_matrix_t m = d * m_generators[i];
            ret(i) = m.real().trace() * 1.0/m_num_levels;
        }
        return ret;
    }

public:
    explicit rv_representation(std::shared_ptr<const qm_description> qm_desc) :
        m_num_levels(qm_desc->get_num_levels()),
        m_num_adj(qm_desc->get_num_levels() * qm_desc->get_num_levels()),
        m_hamiltonian(m_num_adj, m_num_adj),
        m_dipole_op(m_num_adj, m_num_adj),
        /* NOTE don't need anymore? */
        m_dipole_op_vec(m_num_adj, 1),
        m_relax_superop(m_num_adj, m_num_adj)
    {
        /* set up generators of Lie algebra su(num_levels) */
        setup_generators();
        /* calculate Liouvillian of Hamiltonian */
        m_hamiltonian = calc_liouvillian(qm_desc->get_hamiltonian());
        /* calculate Liouvillian of dipole moment operator */
        m_dipole_op = calc_liouvillian(qm_desc->get_dipole_operator());
        /* calculate dipole moment operator in vector form */
        m_dipole_op_vec = calc_op_vec(qm_desc->get_dipole_operator());
        /* calculate superoperator in redundant (half-vector) vector representation */
        m_relax_superop =
            calc_relaxation_superop(qm_desc->get_relaxation_superop());
    }

    /**
     * Gets the contribution of the Hamiltonian to the Liouvillian
     * \f$ \mathcal{L} = -\mathrm{i}\hbar^{-1} \left[ H, \cdot \right] \f$.
     */
    const real_matrix_t&
    get_hamiltonian() const
    {
        return m_hamiltonian;
    }

    /**
     * Gets the contribution of the dipole moment operator to the
     * Liouvillian
     * \f$ \mathcal{L} = -\mathrm{i}\hbar^{-1} \left[ \mu, \cdot \right] \f$.
     */
    const real_matrix_t&
    get_dipole_operator() const
    {
        return m_dipole_op;
    }

    /**
     * Gets the dipole moment operator in vector form.
     */
    const real_matrix_t&
    get_dipole_operator_vec() const
    {
        return m_dipole_op_vec;
    }

    /**
     * Gets relaxation superoperator in coherence vector representation.
     */
    const real_matrix_t&
    get_relaxation_superop() const
    {
        return m_relax_superop;
    }

    /**
     * Calculates initial coherence vector.
     */
    real_matrix_t
    get_initial_vec(const qm_operator& rho_init) const
    {
        return calc_op_vec(rho_init);
    }

    /**
     * Calculates population \f$ \rho_{mm} \f$ for a given coherence vector
     * \param d and index \param m.
     */
    template<unsigned int num_lvl, unsigned int num_adj>
    static real
    calc_population(const Eigen::Matrix<real, num_adj, 1>& d, unsigned int m)
    {
        /* TODO How to get the populations from the vector ?
         * in SU(NxN) the last N entries of the vector is the diagonal
         * */
       int idx = num_adj - num_lvl + m;
       return d(idx);
    }

};

}

#endif
