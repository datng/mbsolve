/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#ifndef MBSOLVE_ALGO_LINDBLAD_CVR_PADE_H
#define MBSOLVE_ALGO_LINDBLAD_CVR_PADE_H

#include <numeric>
#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>
#include <Eigen/StdVector>
#include <unsupported/Eigen/MatrixFunctions>
#include <internal/coherence_vector_representation.hpp>

namespace mbsolve {

/* TODO outsource to cvr class instead */
/**
 * Base class for all algorithms that solve the Lindblad equation in
 * coherence vector representation.
 * Use only internally to implement solvers.
 * \ingroup MBSOLVE_LIB_INT
 */
template<unsigned int num_lvl>
class lindblad_cvr_base
{
protected:

public:
};


#if 0
/**
 * Solves the Lindblad equation in coherence vector representation using the
 * Pade matrix exponential method.
 * Use only internally to implement solvers.
 * \ingroup MBSOLVE_LIB_INT
 */
template<unsigned int num_lvl>
class lindblad_cvr_pade : public lindblad_cvr_base<num_lvl>
{
private:
    static const unsigned int vec_len = num_lvl * num_lvl - 1;

    typedef Eigen::Matrix<real, vec_len, vec_len> real_matrix_t;

public:
    static std::string name() { return "cvr-pade"; }

    /* TODO outsource to cvr class */
    typedef Eigen::Matrix<real, vec_len, 1> density;

    static density
    get_density_zero() {
        return density::Zero();
    }

    /* TODO random? */
    static density
    get_density_random() {
        return density::Zero();
    }

#if 0
    /* TODO this is a quick and dirty fix and inefficient */
    static density
    get_density_init(const qm_operator& rho) {
        density ret = density::Zero();
        ret(3) = -1;
        return ret;
            //cvr.get_initial_vec(rho);
    }
#endif

    static real
    calc_inversion(const density& d) {
        return d(num_lvl * (num_lvl - 1));
    }

    static inline real
    calc_population(const density& d, unsigned int idx) {
        return cv_representation::calc_population<num_lvl, vec_len>(d, idx);
    }
    /* end TODO outsource */

    class sim_constants {
    public:
        /* does material even have a quantum mechanical description? */
        bool has_qm;

        /* time-independent contribution in cvr
         * (time-independent part of Hamiltonian plus dissipation
         * superoperator contribution)
         */
        real_matrix_t M;

        /* dipole moment operator in cvr */
        real_matrix_t U;

        /* equilibrium vector term */
        density d_eq;

        /* initial density matrix */
        density d_init;
    };

    typedef Eigen::aligned_allocator<sim_constants> allocator;

    static sim_constants
    get_qm_constants(std::shared_ptr<const qm_description> qm,
                     real time_step, const qm_operator& rho_init) {
        sim_constants sc;

        /* quantum mechanical description present? */
        if (qm) {
            /* check whether number of levels matches solver */
            if (qm->get_num_levels() != num_lvl) {
                throw std::invalid_argument("Number of energy levels does not "
                                            "match selected solver!");
            }



            sc.d_init = density::Zero();

        } else {
            /* no quantum mechanical description */


            sc.d_init = density::Zero();
        }

        return sc;
    }

    static void
    update_polarization(const sim_constants &sc, real *p_t, const density& d) {

    }

    static void
    update_density(const sim_constants &sc, density& d, real e) {


    }

};
#endif

}

#endif




#if 0

    bool has_qm;
    bool has_dipole;

    /* constant propagator A_0 = exp(M dt/2) */
    real_matrix_t A_0;

    /* unitary transformation matrix */
    complex_matrix_t B;

    /* required for polarization calc ? */
    real_matrix_t M;
    real_matrix_t U;
    real_vector_t d_in;
    real_vector_t d_eq;

    /* dipole moments */
    real_vector_t v;

    /* diagonalized interaction propagator */
    /* TODO: special type for diagonal matrix? */
    /* TODO: vector would do, right? */
    Eigen::Matrix<complex, num_adj, 1> L;

    real d_t;

    /* initialization constants */
    real_vector_t d_init;

};


        /* rodrigues formula precalc */
        real_matrix_t U2;
        real theta_1;

        /* constant propagator A_0 = exp(M dt/2) */
        real_matrix_t A_0;

        /* unitary transformation matrix */
        complex_matrix_t B;

        /* required for polarization calc ? */


        real_vector_t d_in;


        /* dipole moments */
        real_vector_t v;

        /* diagonalized interaction propagator */
        /* TODO: special type for diagonal matrix? */
        /* TODO: vector would do, right? */
        Eigen::Matrix<complex, num_adj, 1> L;
#endif
#if 0

complex mexp(const complex& arg)
{
    return std::exp(arg);
}

template<unsigned int num_lvl, unsigned int num_adj>
inline Eigen::Matrix<real, num_adj, num_adj>
mat_exp(const sim_constants_clvl_os<num_lvl>& s, real e)
{
    Eigen::Matrix<real, num_adj, num_adj> ret;

#if EXP_METHOD==1
    /* by diagonalization */
    Eigen::Matrix<complex, num_adj, 1> diag_exp = s.L * e;
    diag_exp = diag_exp.unaryExpr(&mexp);

    ret = (s.B * diag_exp.asDiagonal() * s.B.adjoint()).real();
#elif EXP_METHOD==2
#else
    /* Eigen matrix exponential */
    ret = (s.U * e * s.d_t).exp();
#endif

    return ret;
}

#endif

#if 0
    /* diag:

       Eigen::ComplexSchur<Eigen::Matrix<real, num_adj, num_adj> >
            schur_c(U);

       sc.B = schur_c.matrixU();

         /* store diagonal matrix containing the eigenvalues */
            sc.L = schur_c.matrixT().diagonal() * scen->get_timestep_size();
#endif
