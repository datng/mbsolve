/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#ifndef MBSOLVE_NEWTON_PROPAGATOR_H
#define MBSOLVE_NEWTON_PROPAGATOR_H

// #define EIGEN_USE_MKL_ALL

#include <Eigen/Core>
#include <unsupported/Eigen/MatrixFunctions>
#include <internal/redundant_vector_representation.hpp>
#include <iostream>
#include <Eigen/StdVector>
#include <Eigen/Eigenvalues>
// #include <mkl.h>

namespace mbsolve {
/**
 * Implementatio of the Newton Propagator with Restarted Arnoldi from
 * [1] "Optimizing Robust Quantum Gates in Open Quantum Systems, Doctoral Dissertation
 * by M. Goerz in 2015. URL: https://michaelgoerz.net/
 * The method is described on page 51-54 and the pseudo code is
 * presented on pages 187-191 (Appendix F)
 *
 */

template<unsigned int num_lvl>
class newton_propagator {

    /**
     * The accuracy we want to achieve - machine accuracy
     */
    static constexpr real tol = std::numeric_limits<real>::epsilon();

    static const unsigned int vec_len = num_lvl * num_lvl;
    /**
     * Type definitions. Matrices are for now statically allocated.
     * For number of levels bigger than 6, which produce 36x36 matrices,
     * dynamically allocated Eigen::MatrixXd should be used instead.
     * Reference: https://eigen.tuxfamily.org/dox/group__TutorialMatrixClass.html
     * under "Fixed vs. Dynamic size"
     */
    typedef Eigen::Matrix<real, vec_len, 1> density_t;
    typedef Eigen::Matrix<complex, vec_len, 1> complex_density_t;
    typedef Eigen::MatrixXd real_matrix_t;
    typedef Eigen::MatrixXcd complex_matrix_t;
    typedef std::vector<complex> complex_dyn_vec_t;
    typedef std::vector<complex_density_t,  Eigen::aligned_allocator<complex_density_t>> vec_density_t;


public:

    newton_propagator() {}

    /**
     * @param [in] A liouvillian Superoperator matrix N^2 x N^2
     * @param [in] v density vector N^2 x 1
     * @param [in] dt time step
     * @param [in] m maximal size of Hessenberg matrices (TODO what exactly should this be?)
     * @param [in] U return - extended Arnoldi vectors
     * @param [in] H return - Hessenberg matrix of order (m+1)x(m+1)
     * @param [in] Z return - interpolation points vector
     * @param [in] m return - new m < m_max
     */
    static complex_matrix_t arnoldi(real_matrix_t& A, complex_density_t& v0, const unsigned int m_max,
                    vec_density_t& U, complex_dyn_vec_t& Z, unsigned int& m, double dt)
    {
        /* return Hessenberg matrix */
        complex_matrix_t H = Eigen::MatrixXcd::Zero(m_max+1,m_max+1);
        H *= 0.;
        real beta = v0.norm();
        /* v is the normalized v0 vector */
        complex_density_t v = complex_density_t::Zero(vec_len,1);
        v = v0/beta;
        m = m_max;

        U.push_back(v);
        for (int j=0; j<m_max; ++j) {
            v = A * v;
            for (int i = 0; i < U.size(); ++i) {
                H(i, j) = dt * U[i].dot(v);
                v -= (H(i, j) * 1./dt) * U[i];
            }
            /* find eigenvalues of the hessenberg matrix
             * and add them to Z
             * */
            Eigen::VectorXcd evalues = H.block(0, 0, j + 1, j + 1).eigenvalues();
            for (int i = 0; i <= j; ++i) {  // the number of eigenvectors depends on the size of the matrix!
                Z.push_back(evalues(i));   // Is it correct this way?
            }

            real h_next = v.norm();
            H(j + 1, j) = h_next * dt;
            if (h_next <= 1.0e-14) {
                /* Python reference m = j.  */
                m = j;
                break;
            }
            v *= 1. / h_next; // normalisieren
            /* Do I need to check for duplication ?*/
            U.push_back(v);
        }
        return H.block(0, 0, m + 1,m + 1);
    }

    /**
     * Helper function.
     * Returns the element with the maximal absolute value for a
     * std::vector<std::complex<double>>
     * Eigen: z_vec.array().abs().maxCoeff(&idx)
     */
    static complex max_abs(complex_dyn_vec_t& Z, int& idx) {
        real tmp, abs_v = -1.;
        for (int i=0; i < Z.size(); ++i) {
            tmp = std::abs(Z[i]);
            if (tmp > abs_v) {
                idx = i;
                abs_v = tmp;
            }
        }
        return Z[idx];
    }


    /**
     * Extend Leja points (ordered_points) with m new points from the set of the
     * Ritz values (new_points). Adds points directly to ordered_points and removes the added
     * points from new_points.
     */
    static void extendLeja(complex_dyn_vec_t& ordered_points,
                    complex_dyn_vec_t& new_points, const unsigned int m) {
        int n_added = 0, n_old = ordered_points.size();
        int i_add_start = 0;

        if (n_old == 0) {
            int idx = -1;
            complex z = max_abs(new_points, idx);
            ordered_points.push_back(z);
            /* copy the point to the back and then erase -> no reallocation needed */
            complex tmp = new_points.back();
            new_points.back() = z;
            new_points[idx] = tmp;
            new_points.pop_back();
            ++n_added;
            ++i_add_start;
        }


        real prod_max{0.}, tmp_prod{1.};
        int idx{0};
        complex z;
        for (int n=n_added; n < m; ++n) {
            prod_max = 0.; tmp_prod = 1.;
            idx = 0;
            for (int i = 0; i < new_points.size(); ++i) {
                /* For every point in new_points */
                tmp_prod = 1.;
                z = new_points[i];
                for (int j = 0; j < ordered_points.size(); ++j) {
                    /* For every point in ordered_points
                    -> tmp_prod *= std::pow(abs(z-ordered_points[j]), (1./(m+n_old)));*/
                    tmp_prod *= abs(z-ordered_points[j]);
                }
                if (tmp_prod > prod_max) {
                    prod_max = tmp_prod;
                    idx = i;
                }
            }
            /* copy the point to the back and delete, to
               avoid reallocation */
            ordered_points.push_back(new_points[idx]);
            complex tmp = new_points.back();
            new_points.back() = new_points[idx];
            new_points.pop_back();
        }
    }

    /**
     * Define the (scalar) function that is being approximated...
     */
    inline static complex exponential(const complex& z) {
        return std::exp(z);
    }

    inline static real exponential(const real& d) {
        return std::exp(d);
    }
    /**
     *  Calculate and add new Newton Coefficients.
     *  leja points need to be already normalized!
     */
    static void extendNewtonCoeffs(complex_dyn_vec_t& a, complex_dyn_vec_t& leja,
                            const real q_radius, complex c) {
        long unsigned int n_old{a.size()}, nS{leja.size()};
        long unsigned int m{leja.size()-n_old};
        int n0 = n_old;
        if (n_old == 0){
            complex a0 = exponential(leja[0]);
            a.push_back(a0);
            n0 = 1;
        }
        for (int k = n0; k < n_old + m; ++k) {
            complex prod{1.,0.};
            complex sum{0.,0.};
            for (int n = 1; n < k; ++n) { /* start from 1 or 0 ??? */
                complex zd = leja[k] - leja[n-1];
                prod *= zd * q_radius;
                sum += a[n]*prod;
            }
            prod *= (leja[k]-leja[k-1]) * q_radius; /* the last prod should be from j=0 to k-2 */
            complex ak = ( exponential(leja[k]) - a[0] - sum) * 1./prod;
            a.push_back(ak);
        }
    }

    static void normalizeZ(complex& c, complex& ro, complex_dyn_vec_t& Z, int m) {
        for (int i = 0; i < m; ++i) {
            c += Z[i];
        }
        c /= (real)m;
        for (int i = 0; i < m; ++i) {
            ro *= std::pow(abs(c - Z[i]), 1/m);
        }
        for (int i = 0; i < Z.size(); ++i) {
            Z[i] -= c;
            Z[i] /= ro;
        }
    }

    static double normalize_v2(complex_dyn_vec_t& Z) {
        /**
         * goerz github version
         */
        real r{0.};

        for (const auto& z : Z) {
            real r_i = std::abs(z);
            if (r_i > r) {
                r = r_i;
            }
        }
        // make the radius a bit bigger, to account for the next points...
        r *= 1.2;
        if (r <= 0.) {
            std::cout << "Radius not positive! r = " << r << std::endl;
        }
        return r;
    }

    static complex_density_t restarted_newton(density_t& v0, real_matrix_t& A,
                    real dt, int m_max, const unsigned int maxrestart) {
        if (v0.norm() < 1e-18) {
            return complex_density_t::Zero(vec_len,1);
        }
        complex_density_t w = complex_density_t::Zero(vec_len, 1);
        w *= 0.;

        complex_dyn_vec_t Z, Ritz, a;
        Z.clear();
        Ritz.clear();
        a.clear();

        complex_density_t v = complex_density_t::Zero(vec_len,1);
        v = v0;

        real beta = v.norm();
        v /= beta;
        real radius{0.}, q_radius;
        for (int s = 0; s < maxrestart; ++s) {
            vec_density_t U;
            unsigned int m = m_max;
            Eigen::MatrixXcd H = arnoldi(A, v, m_max, U, Ritz, m, dt);

            if (m == 0 && s == 0) {
                return exponential(beta * H(0,0))*v;
            }

            /* normalize Ritz points */
            real center{0.};
            if (s == 0) {
                radius = normalize_v2(Ritz);
                q_radius = 1./radius;
            }
            /* get leja points */
            int n_s = Z.size();
            extendLeja(Z, Ritz, m);
            extendNewtonCoeffs(a, Z, q_radius, center);

            Eigen::VectorXcd R = Eigen::VectorXcd::Zero(m + 1, 1);
            Eigen::VectorXcd P = Eigen::VectorXcd::Zero(m + 1, 1);
            P *= 0.; R *= 0.;
            R(0) = complex{beta, 0.};
            P(0) = a[n_s] * complex{beta, 0.}; // P = a[n_s] * R;
            for (int k = 1; k < m; ++k) {
                R = ( H * R - Z[n_s + k -1] * R ) * q_radius;
                P += a[n_s + k] * R;
            }

            complex_density_t wp = complex_density_t::Zero(vec_len, 1);
            wp *= 0.;
            for (int i = 0; i < m; ++i) {
                wp += P(i) * U[i];
            }

            w += wp;

            R = ( H*R - Z[n_s + m -1]*R ) * q_radius;
            beta = R.norm();
            R /= beta;
            v = complex_density_t::Zero(vec_len, 1);
            for (int i = 0; i < m+1; ++i) {
                v += R(i) * U[i];
            }

            if (beta * std::abs(a.back()) / (1 + w.norm()) < tol) {
                break;
            }
            if (s == maxrestart-1) {
                std::cout << "Didn't reach convergence!" << std::endl;
            }
        }

        return w;

    }

}; // namespace newton propagator

} // namespace mbsolve

#endif
