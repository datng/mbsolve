/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#ifndef MBSOLVE_CW_SOLVER_H
#define MBSOLVE_CW_SOLVER_H

#include <map>
#include <memory>
#include <string>
#include <vector>
#include <device.hpp>
#include <scenario.hpp>
#include <sim_result.hpp>
#include <types.hpp>

namespace mbsolve
{

/**
 * An abstract parent class for all individual solvers.
 */

class cw_solver
{
private:
	std::string m_name; /**< Name of the solver. */

protected:
	std::shared_ptr<device> m_device; /**< Shared Pointer to the device. */

	std::shared_ptr<scenario> m_scenario; /**< Shared Pointer to the scenario. */

    /**
     * Shared Pointer to a vector with all simulation results.
     */
	std::vector<std::shared_ptr<sim_result> > m_results;

public:
    /**
     * Constructs cw_solver with a given \p name.
     *
     * \param [in] name Name of the solver method.
     * \param [in] dev  Specify the \ref device to be simulated.
     * \param [in] scen Specify the \ref scenario.
     */
    cw_solver(const std::string& name, std::shared_ptr<device> dev,
	   std::shared_ptr<scenario> scen) :
        m_name(name), m_device(dev), m_scenario(scen){}

    cw_solver() : m_name(""){}

    ~cw_solver(){}

    /**
     * Gets solver name.
     */
    const std::string& get_name() const { return m_name; }

    /**
     * Gets scenario.
     */
    const std::shared_ptr<scenario> get_scenario() const { return m_scenario; }

    /**
     * Gets device.
     */
    const std::shared_ptr<device> get_device() const { return m_device; }

    /**
     * Executes the solver. Virtual method must be overwritten by child classes.
     */
    virtual void run() const = 0;

    /**
     * Gets results.
     */
    const std::vector<std::shared_ptr<sim_result> >& get_results() const
    { return m_results; }

};

}

#endif
