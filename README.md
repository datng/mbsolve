# mbsolve
An open-source solver tool for the Maxwell-Bloch equations.

Overview
--------------------

The project consists of the following components:

- mbsolve-lib: Contains the common base classes for solvers and writers (which
are subsequently derived and implemented in the other subprojects). As the
name suggests, the solver class is responsible for solving the Maxwell-Bloch
equations while the writer class writes the results to a file of the
respective format. It also provides classes to describe a device setup
(materials, dimensions, ...) and a simulation scenario (simulation settings,
sources, ...).

- mbsolve-tool: A C++ test application that implements various simulation
setups.

- solver-cuda: Uses NVIDIA's CUDA framework to efficiently solve the equation
system on graphics processing units (GPUs). Different solvers are implemented
in this subproject.

- solver-generic: A no-op test solver for testing purposes only. May be
discarded in future.

- solver-cpu: Uses MPI and/or OpenMP to solve the equation system on clusters
  with distributed and/or shared memory.

- tools: Various scripts in different scripting languages.

- writer-hdf5: Writes the results into the HDF5 format.

- writer-matlab: Writes the results into the MATLAB file format (deprecated).

<img src="doc/svg/mbsolve_overview.png" alt="mbsolve overview" width="500"/>

All libraries feature a Python interface and can be used conveniently from
Python scripts, Jupyter notebooks etc. See tools/python for examples.

Build instructions
--------------------
The project is built using the CMake build system. The typical commands under
Linux are

    $ cd mbsolve
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ ./mbsolve [--options]

By default, the build type is set to release with debug information, which
enables debugging but features the optimization necessary for reasonable
performance.

In order to enable/disable a component, an extra option has to be passed to
CMake:

    $ cmake -DWITH_CUDA=ON/OFF ..

It is possible to build the cpu-solver only with OpenMP:

    $ cmake -DWITH_OPENMP=ON

At the moment, it is not possible to build the cpu-solver only with MPI and
without OpenMP. Hence, building with

    $ cmake -DWITH_MPI=ON

will in fact be the same as

    $ cmake -DWITH_HYBRID=ON

Just remember to set

    $ OMP_NUM_THREADS=1

when running the MPI cpu-solver (see below).

The project has been successfully compiled with the GNU compiler collection
(gcc) and the Intel compiler (icc). gcc is used as default, the Intel compiler
may be used with the following command:

    $ CC=path_to_intel/icc CXX=path_to_intel/icpc cmake -DWITH_CUDA=OFF ..

Note that CUDA does not support all versions of the Intel compiler, hence the
CUDA support is disabled.

The target architecture may be set using the ARCH option. By default, compiler
optimizations suitable for the host machine are applied. However, when
building for another machine the architecture can be set with e.g.

    $ cmake -DARCH=AVX2 ..

In particular, building for Intel's Xeon Phi coprocessor is enabled with

    $ CC=path_to_intel/icc CXX=path_to_intel/icpc cmake -DWITH_CUDA=OFF \
      -DARCH=PHI ..

The Doxygen documentation can be build with

    $ make doc

If you want to use Eclipse, use the appropriate generator flag -G and (if
desired) set the build type to Debug. (Note that Eclipse yields a warning if
the build directory is a child of the source directory. Adjust path_src.)

    $ cmake -DCMAKE_BUILD_TYPE=Debug -G"Eclipse CDT4 - Unix Makefiles" path_src

Run the program
--------------------
To execute the cpu solver on a system with 2 nodes and 28 cores per node,
run

    $ OMP_NUM_THREADS=1 mpiexec -n 56 -ppn 28 path/to/mbsolve-tool [options]

for a pure MPI version of the solver. This is recommended for high node
counts > 16.
For a pure OpenMP version on one node, run

    $ OMP_NUM_THREADS=28 mpiexec -n 1 path/to/mbsolve-tool [options]

For a mixed version of MPI and OpenMP und 2 nodes run

    $ OMP_NUM_THREADS=28 mpiexec -n 2 -ppn 1 path/to/mbsolve-tool [options]

This is recommended for lower node counts.
To pin the OpenMP threads, it is recommended to set the following environment
variables:

    $ OMP_PLACES=cores
    $ OMP_PROC_BIND=close

To pin the MPI processes, use the following intel-specific environment
variables:

    $ I_MPI_PIN=1
    $ I_MPI_PIN_PROCESSOR_LIST=allcores

If you compiled the program with MPI support, bare in mind that running the
program without `mpiexec` will fail.

Tools
--------------------
- Python scripts: Example scripts that define simulation setups and run the
  solver. Execute from build directory. See tools/python.

- MATLAB scripts: The examples read the simulation results and plot them.
  See tools/matlab.

- Loadleveler: Example scripts for the loadleveler batch scheduler.
  See tools/loadleveler.

Dependencies
--------------------
If the required dependencies are not met, the components may be disabled by
the build system. The dependencies are listed in the following with the
minimum version given in brackets:

- CMake (3.6)
- Eigen library (3.3.4)
- Python (2.7) and SWIG (2.0) for Python support
- MATLAB (any recent version) for matlab-writer
- CUDA (7.0) for solver-cuda
- MPI and OpenMP (any recent version) for solver-cpu
- mpi4py (any recent version) for the python interface of solver-cpu
- Boost (any recent version) for mbsolve-tool
- Doxygen (any recent version) for the documentation

Contributing to mbsolve
--------------------
Feel free to use the code and do not hesitate to contact me or to create an
issue on github. Any contributions are welcome. If you found the code helpful
and used it in your research, you can cite the following paper:<br />
M. Riesch, N. Tchipev, S. Senninger, H.-J. Bungartz, and C. Jirauschek,
"Performance evaluation of numerical methods for the
Maxwell-Liouville-von Neumann equations," Opt. Quant. Electron. <b>50</b>,
112 (2018).
<a href="https://www.riesch.at/michael/publications/riesch2018oqel.bib">BIB</a>
<a href="https://www.riesch.at/michael/publications/riesch2018oqel.pdf">PDF</a>
<a href="http://dx.doi.org/10.1007/s11082-018-1377-4">DOI</a>
