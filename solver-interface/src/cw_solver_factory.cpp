/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#if !defined(USE_MPI)
# error "You should specify USE_MPI=0 or USE_MPI=1 on the compile line"
#endif

#include <map>
#include <memory>
#include <string>
#include <vector>
#include <device.hpp>
#include <scenario.hpp>
#include <types.hpp>
#include <cw_solver.hpp>

#if USE_MPI
#include <mpi.h>
#include <cw_solver_factory_mpi.hpp>
#else
#include <cw_solver_factory.hpp>
#endif

#include <internal/algo_lindblad_cvr_rodr.hpp>
#include <../../solver-cpu/include/solver_cpu_fdtd_red.hpp>
#include <internal/algo_lindblad_rvr_action.hpp>
#include <internal/algo_lindblad_rvr_action_static.hpp>
#include <internal/algo_lindblad_rvr_newton.hpp>
#include <../../solver-generic/include/solver_generic.hpp>

namespace mbsolve
{
	std::shared_ptr<cw_solver>
	cw_solver_factory::create_solver(std::string name, std::shared_ptr<device> dev,
                                     std::shared_ptr<scenario> scen,
                                     std::string init_method){

        std::shared_ptr<cw_solver> solver;
        if(name == "cpu-fdtd-red-2lvl-cvr-rodr"){
            solver = std::make_shared<solver_cpu_fdtd_red<2,
                                            lindblad_cvr_rodr> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-3lvl-cvr-rodr"){
            solver = std::make_shared<solver_cpu_fdtd_red<3,
                                            lindblad_cvr_rodr> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-6lvl-cvr-rodr"){
            solver = std::make_shared<solver_cpu_fdtd_red<6,
                                            lindblad_cvr_rodr> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-10lvl-cvr-rodr"){
            solver = std::make_shared<solver_cpu_fdtd_red<10,
                                            lindblad_cvr_rodr> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-2lvl-rvr-action"){
            solver = std::make_shared<solver_cpu_fdtd_red<2,
                                            lindblad_rvr_action> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-3lvl-rvr-action"){
            solver = std::make_shared<solver_cpu_fdtd_red<3,
                                            lindblad_rvr_action> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-6lvl-rvr-action"){
            solver = std::make_shared<solver_cpu_fdtd_red<6,
                                            lindblad_rvr_action> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-10lvl-rvr-action"){
            solver = std::make_shared<solver_cpu_fdtd_red<10,
                                            lindblad_rvr_action> >(dev, scen,
                                                                init_method);
        }
        // uncommented due to stack size problem
        /*else if(name == "cpu-fdtd-red-2lvl-rvr-action_static"){
            solver = std::make_shared<solver_cpu_fdtd_red<2,
                                    lindblad_rvr_action_static> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-3lvl-rvr-action_static"){
            solver = std::make_shared<solver_cpu_fdtd_red<3,
                                    lindblad_rvr_action_static> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-6lvl-rvr-action_static"){
            solver = std::make_shared<solver_cpu_fdtd_red<6,
                                    lindblad_rvr_action_static> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-10lvl-rvr-action_static"){
            solver = std::make_shared<solver_cpu_fdtd_red<10,
                                    lindblad_rvr_action_static> >(dev, scen,
                                                                init_method);
        }*/
        else if(name == "cpu-fdtd-red-2lvl-rvr-newton"){
            solver = std::make_shared<solver_cpu_fdtd_red<2,
                                            lindblad_rvr_newton> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-3lvl-rvr-newton"){
            solver = std::make_shared<solver_cpu_fdtd_red<3,
                                            lindblad_rvr_newton> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-6lvl-rvr-newton"){
            solver = std::make_shared<solver_cpu_fdtd_red<6,
                                            lindblad_rvr_newton> >(dev, scen,
                                                                init_method);
        }
        else if(name == "cpu-fdtd-red-10lvl-rvr-newton"){
            solver = std::make_shared<solver_cpu_fdtd_red<10,
                                            lindblad_rvr_newton> >(dev, scen,
                                                                init_method);
        }
        else {
			throw std::invalid_argument("Unknown solver " + name);
		}
        return solver;
	}

#if USE_MPI
	/**
	 * Function needed for setting a custom MPI_Comm, e.g. for using the python interface
	 */
	std::shared_ptr<cw_solver>
	cw_solver_factory::create_mpi_solver(std::string name, std::shared_ptr<device> dev,
										std::shared_ptr<scenario> scen,
                                        std::string init_method, MPI_Comm comm){

		if(name == "cpu-fdtd-red-6lvl-cvr-rodr"){
			auto sol = std::make_shared<solver_cpu_fdtd_red<6,
                                lindblad_cvr_rodr> >(dev, scen, init_method,
                                                                    comm);
			return sol;
		}
		else {
			throw std::invalid_argument("Unknown solver " + name);
		}
	}
#endif
}
