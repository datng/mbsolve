%module pysolverinterface

%include "exception.i"
%include "std_except.i"
%include "std_map.i"
%include "std_shared_ptr.i"
%include "std_string.i"
%include "std_vector.i"

%{
#include "../include/cw_solver_factory.hpp"

using namespace mbsolve;
%}

%import(module="pymbsolvelib") "../../mbsolve-lib/include/mbsolve.hpp"

%exception {
  try {
    $action
  } catch (const std::exception& e) {
    SWIG_exception(SWIG_RuntimeError, e.what());
  }
}

%shared_ptr(mbsolve::device)
%shared_ptr(mbsolve::scenario)
%shared_ptr(mbsolve::cw_solver_factory)

%include "../../mbsolve-lib/include/device.hpp"
%include "../../mbsolve-lib/include/scenario.hpp"
%include "../../mbsolve-lib/include/cw_solver.hpp"
%include "../include/cw_solver_factory.hpp"
