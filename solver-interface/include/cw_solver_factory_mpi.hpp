/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#ifndef MBSOLVE_CW_SOLVER_FACTORY_MPI_H
#define MBSOLVE_CW_SOLVER_FACTORY_MPI_H

#include <mpi.h>

#include <map>
#include <memory>
#include <string>
#include <vector>
#include <device.hpp>
#include <scenario.hpp>
#include <types.hpp>
#include <cw_solver.hpp>

namespace mbsolve
{

/**
 * Factory class, which produces the requested cw_solver.
 * \ingroup MBSOLVE_SOLVER_INTERFACE
 */
class cw_solver_factory
{
public:
    /**
     * Creates a cw_solver requested by its name.
     *
     * \param [in] name The name of the \ref cw_solver.
     * \param [in] dev Shared Pointer to the \ref device.
     * \param [in] scen Shared Pointer to the \ref scenario.
     */
	static std::shared_ptr<cw_solver>
	create_solver(std::string name, std::shared_ptr<device> dev,
			std::shared_ptr<scenario> scen, std::string init_method);

    /**
     * Creates a cw_solver requested by its name.
     * This method must be used to set a special MPI Communicator at the solver,
     * as needed py the python interface.
     *
     * \param [in] name The name of the \ref cw_solver.
     * \param [in] dev Shared Pointer to the \ref device.
     * \param [in] scen Shared Pointer to the \ref scenario.
     * \param [in] comm The MPI Communicator, which will be used by the cw_solver.
     */
	static std::shared_ptr<cw_solver>
	create_mpi_solver(std::string name, std::shared_ptr<device> dev,
			std::shared_ptr<scenario> scen, std::string init_method, MPI_Comm comm);
};

}

#endif
