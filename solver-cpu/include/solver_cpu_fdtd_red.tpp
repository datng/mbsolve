/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#ifndef MBSOLVE_SOLVER_CPU_FDTD_RED_TPP
#define MBSOLVE_SOLVER_CPU_FDTD_RED_TPP

#if !defined(USE_MPI)
# error "You should specify USE_MPI=0 or USE_MPI=1 on the compile line"
#endif

#define EIGEN_DONT_PARALLELIZE
#define EIGEN_NO_MALLOC
#define EIGEN_STRONG_INLINE inline

#if USE_MPI
#include <mpi.h>
#endif
#include <iostream>
// TODO: make omp optional
#include <omp.h>
#include <random>
#include <common_cpu.hpp>
#include <solver_cpu_fdtd_red.hpp>
#include <internal/algo_lindblad_cvr_pade.hpp>
#include <internal/algo_lindblad_cvr_rodr.hpp>
#include <internal/algo_lindblad_noop.hpp>
#include <internal/algo_lindblad_reg_cayley.hpp>
#include <internal/algo_lindblad_rvr_action.hpp>
#include <internal/algo_lindblad_rvr_action_static.hpp>
#include <internal/algo_lindblad_rvr_newton.hpp>
#include <math.h>

#include <device.hpp>
#include <scenario.hpp>

namespace mbsolve {
/* TODO make this flexible and useful */
const unsigned int VEC = 4;

// TODO: cannot be defined in header due to problem when using as template param
/* Tuning parameter: overlap per MPI process */
const unsigned int OL_node = 1;

/* solver class member functions */

#if USE_MPI
template<unsigned int num_lvl, template<unsigned int> class density_algo>
solver_cpu_fdtd_red<num_lvl, density_algo>::solver_cpu_fdtd_red
(std::shared_ptr<device> dev, std::shared_ptr<scenario> scen, const
std::string init_method, MPI_Comm comm):
    cw_solver("cpu-fdtd-red-" + std::to_string(num_lvl) + "lvl-" +
               density_algo<num_lvl>::name(), dev, scen),
    OL_thread((OL_node/1)), m_init_method(init_method)
{
    mpi_comm = comm;
    init();
}
#endif

template<unsigned int num_lvl, template<unsigned int> class density_algo>
solver_cpu_fdtd_red<num_lvl, density_algo>::solver_cpu_fdtd_red
(std::shared_ptr<device> dev, std::shared_ptr<scenario> scen, const std::string
init_method):
    cw_solver("cpu-fdtd-red-" + std::to_string(num_lvl) + "lvl-" +
               density_algo<num_lvl>::name(), dev, scen),
    //OL_thread((num_lvl >= 6 ? 1 : 8))
    OL_thread((OL_node/1)), m_init_method(init_method)
{
    init();
}

template<unsigned int num_lvl, template<unsigned int> class density_algo>
void
solver_cpu_fdtd_red<num_lvl, density_algo>::init()
{
    /* TODO: scenario, device sanity check */
    /*
     * device.length > 0 (-> regions.size() > 0)
     * required materials found?
     * no gap in regions
     *
     */

    /* TODO: solver params
     * courant number
     */
    if(OL_node < OL_thread){
        throw std::invalid_argument("OL_node must be larger than or equal to OL_thread");
    }

#if USE_MPI
    if(mpi_comm == MPI_COMM_NULL){
        mpi_comm = MPI_COMM_WORLD;
    }
    MPI_Comm_size(mpi_comm, &mpi_size);
    MPI_Comm_rank(mpi_comm, &mpi_rank);
#else
    mpi_size = 1;
    mpi_rank = 0;
#endif
    /* report number of used OpenMP threads */
    num_threads = omp_get_max_threads();
    /* report overlap parameter */
    if (mpi_rank == 0){
        std::cout << "MPI overlap parameter: " << OL_node << std::endl;
        std::cout << "OpenMP overlap parameter: " << OL_thread << std::endl;
        std::cout << "Number of MPI processes: " << mpi_size << std::endl;
        std::cout << "Number of threads per MPI process: " << num_threads << std::endl;
    }

    uint64_t num_gridpoints = m_scenario->get_num_gridpoints();
    if(((num_gridpoints / mpi_size) + 2 * OL_node) / num_threads <=
                                                    OL_node + 2 * OL_thread){
        std::cout << "Invalid Parallel Combination" << std::endl;
        throw std::invalid_argument(
            "OL_node is too large for this combination"
                                   " of parallelization and OL_thread.");
    }

    if (m_device->get_regions().size() == 0) {
        throw std::invalid_argument("No regions in device!");
    }

    /* determine simulation settings */
    init_fdtd_simulation(m_device, m_scenario, 0.5);
    /* inverse grid point size for Ampere's law */
    m_dx_inv = 1.0/m_scenario->get_gridpoint_size();

    /* set up simulation constants for quantum mechanical description */
    std::vector<sim_constants_fdtd> m_sim_consts_fdtd;
    std::map<std::string, unsigned int> id_to_idx;
    unsigned int j = 0;
    for (const auto& mat_id : m_device->get_used_materials()) {
        auto mat = material::get_from_library(mat_id);
        /* constants for electromagnetic (optical) field */
        m_sim_consts_fdtd.push_back(get_fdtd_constants(m_device, m_scenario, mat));

        /* constants for quantum mechanical system */
        m_sim_consts_qm.push_back(density_algo<num_lvl>::get_qm_constants
                                  (mat->get_qm(), m_scenario->get_timestep_size()));
        /* enter material index */
        id_to_idx[mat->get_id()] = j;
        j++;
    }
    m_source_data_threads = new real*[num_threads];

#pragma omp parallel
    {
        unsigned int tid = omp_get_thread_num();
        m_source_data_threads[tid] = (real *) mb_aligned_alloc(m_scenario->get_num_timesteps() *
                                    m_scenario->get_sources().size() * sizeof(real));
        /* create source data */
        unsigned int base_idx = 0;
        for (const auto& src : m_scenario->get_sources()) {
            sim_source s;
            s.type = src->get_type();
            s.x_idx = src->get_position()/m_scenario->get_gridpoint_size();
            s.data_base_idx = base_idx;
            if(tid == 0) m_sim_sources.push_back(s);

            /* calculate source values */
            for (unsigned int j = 0; j < m_scenario->get_num_timesteps(); j++) {
                m_source_data_threads[tid][base_idx + j] =
                    src->get_value(j * m_scenario->get_timestep_size());
            }

            base_idx += m_scenario->get_num_timesteps();
        }
    }

    /* compute and store chunks per node */
    node_chunks = (unsigned int*)malloc(mpi_size * sizeof(unsigned int));
    node_chunks_cumulated = (unsigned int*)malloc(mpi_size * sizeof(unsigned int));
    distribute_chunks(num_gridpoints, 0, mpi_size, node_chunks, node_chunks_cumulated);

    /* compute and store chunks per thread */
    thread_chunks = (unsigned int*)malloc(num_threads * sizeof(unsigned int));
    thread_chunks_cumulated = (unsigned int*)malloc(num_threads * sizeof(unsigned int));
    distribute_chunks(node_chunks[mpi_rank] + 2 * OL_node - 2 * OL_thread,
            node_chunks_cumulated[mpi_rank] + 2 * OL_node - 2 * OL_thread,
            num_threads, thread_chunks, thread_chunks_cumulated);

#if USE_MPI
    // needed for communicating boundary values to neighbors
    requests = (MPI_Request*)malloc((mpi_size - 1) * 4 * sizeof(MPI_Request));
    statuses = (MPI_Status*)malloc((mpi_size - 1) * 4 * sizeof(MPI_Status));
#endif

    /* allocate data arrays */
    typedef typename density_algo<num_lvl>::density density_t;
    m_d = new density_t*[num_threads];
    m_e = new real*[num_threads];
    m_h = new real*[num_threads];
    m_p = new real*[num_threads];
    m_fac_a = new real*[num_threads];
    m_fac_b = new real*[num_threads];
    m_fac_c = new real*[num_threads];
    m_gamma = new real*[num_threads];
    m_mat_indices = new unsigned int*[num_threads];

#pragma omp parallel
    {
        std::random_device rand_dev;
        std::mt19937 rand_gen(rand_dev());
        std::normal_distribution<real> dis(0.0, 1.0);

        unsigned int tid = omp_get_thread_num();
        /* allocation */
        uint64_t size = thread_chunks[tid] + 2 * OL_thread;
        m_d[tid] = new density_t[size];
        m_h[tid] = (real *) mb_aligned_alloc(size * sizeof(real));
        m_e[tid] = (real *) mb_aligned_alloc(size * sizeof(real));
        m_p[tid] = (real *) mb_aligned_alloc(size * sizeof(real));
        m_fac_a[tid] = (real *) mb_aligned_alloc(size * sizeof(real));
        m_fac_b[tid] = (real *) mb_aligned_alloc(size * sizeof(real));
        m_fac_c[tid] = (real *) mb_aligned_alloc(size * sizeof(real));
        m_gamma[tid] = (real *) mb_aligned_alloc(size * sizeof(real));
        m_mat_indices[tid] = (unsigned int *)
            mb_aligned_alloc(size * sizeof(unsigned int));
        for (int i = 0; i < size; i++) {
            int64_t global_idx = node_chunks_cumulated[mpi_rank] +
                                 thread_chunks_cumulated[tid] + i - OL_node;
            real x = global_idx * m_scenario->get_gridpoint_size();

            /* find region and corresponding material */
            int mat_idx = -1;
            bool has_qm = false;
            if ((global_idx >= 0) && (global_idx < num_gridpoints)) {
                for (const auto& reg : m_device->get_regions()) {
                    if ((x >= reg->get_x_start()) && (x <= reg->get_x_end())) {
                        mat_idx = id_to_idx[reg->get_material()->get_id()];
                        has_qm = (reg->get_material()->get_qm()) ?
                            true : false;
                        break;
                    }
                }
            }

            /* initialize data arrays */
            if (mat_idx >= 0) {
                if (has_qm) {
                    m_d[tid][i] = density_algo<num_lvl>::get_density
                        (m_scenario->get_rho_init());
                } else {
                    m_d[tid][i] = density_algo<num_lvl>::get_density();

                }
                if(m_init_method == "zero"){
                    m_e[tid][i] = 0.0;
                }
                else {
                    m_e[tid][i] = dis(rand_gen) * 1e-15;
                }
                m_h[tid][i] = 0.0;
                m_p[tid][i] = 0.0;
                m_fac_a[tid][i] = m_sim_consts_fdtd[mat_idx].fac_a;
                m_fac_b[tid][i] = m_sim_consts_fdtd[mat_idx].fac_b;
                m_fac_c[tid][i] = m_sim_consts_fdtd[mat_idx].fac_c;
                m_gamma[tid][i] = m_sim_consts_fdtd[mat_idx].gamma;
                m_mat_indices[tid][i] = mat_idx;
            } else {
                m_d[tid][i] = density_algo<num_lvl>::get_density();
                m_e[tid][i] = 0.0;
                m_h[tid][i] = 0.0;
                m_p[tid][i] = 0.0;
                m_fac_a[tid][i] = 0.0;
                m_fac_b[tid][i] = 0.0;
                m_fac_c[tid][i] = 0.0;
                m_gamma[tid][i] = 0.0;
                m_mat_indices[tid][i] = 0;
            }
        }
#pragma omp barrier
    }

    /* set up results and transfer data structures */
    compute_scratch_per_node(m_scenario);

}

/*
 * Function, which computes the sizes of the scratch memory per node based
 * on the positions of the sources and the requested records.
 * input:
 *   std::shared_ptr<scenario> scen: the scenario of the setup
 */
template<unsigned int num_lvl, template<unsigned int> class density_algo>
void
solver_cpu_fdtd_red<num_lvl, density_algo>::compute_scratch_per_node
(std::shared_ptr<scenario> scen)
{
    sync_records_sizes = (int*)malloc(mpi_size * sizeof(int));
    sync_records_sizes_cum = (int*)malloc(mpi_size * sizeof(int));
    sync_records_thread_sizes =
            (int*)calloc(num_threads, sizeof(int));
    sync_records_thread_sizes_cum =
            (int*)calloc(num_threads, sizeof(int));

    for(int rank = 0; rank < mpi_size; rank++){
        sync_records_sizes[rank] = 0;
        sync_records_sizes_cum[rank] = 0;
    }

    uint64_t num_gridpoints = scen->get_num_gridpoints();
    uint64_t scratch_offset = 0;
    for (const auto& rec : scen->get_records()) {
        /* create copy list entry */
        copy_list_entry entry(rec, scen, scratch_offset);

        uint64_t position = entry.get_position();
        uint64_t cols = entry.get_cols();
        uint64_t size = entry.get_size();

        /* take imaginary part into account */
        if (rec->is_complex()) {
            size = size * 2;
        }

        for(int rank = 0; rank < mpi_size; rank++){
            double chunk_ratio = node_chunks[rank] * 1.0 / num_gridpoints;
            uint64_t reduced_size = ceil(size * chunk_ratio);

            unsigned int start_idx = node_chunks_cumulated[rank];
            unsigned int end_idx = start_idx + node_chunks[rank];

            // only allocate memory for single column records on that node
            if (position < end_idx &&
                position >= start_idx &&
                cols == 1){
                sync_records_sizes[rank] += size;
                sync_records_sizes_cum[rank] += size;
                scratch_offset += size;
                if(rank == mpi_rank){
                    m_copy_list.push_back(entry);
                }
            }
            // assume that entries with more columns are equally distributed
            // over the grid
            // TODO: maybe improve this assumption
            else if (cols > 1){
                sync_records_sizes[rank] += reduced_size;
                sync_records_sizes_cum[rank] += reduced_size;
                if(rank == mpi_rank){
                    m_copy_list.push_back(entry);
                    scratch_offset += size;
                }
            }

            /* add result to solver */
            if (rank == 0 && mpi_rank == 0){
                m_results.push_back(entry.get_result());
            }
        } /* end rank loop */
    } /* end record loop */

    for(int rank = 0; rank < mpi_size; rank++){
        if(rank > 0){
            sync_records_sizes_cum[rank] = sync_records_sizes_cum[rank - 1];
        }
        else {
            sync_records_sizes_cum[0] = 0;
        }
        // consider 1 dummy element per thread
        sync_records_sizes[rank] += num_threads;
        if(rank == 0){
            sync_records_sizes_cum[rank] = sync_records_sizes_cum[rank]
                - sync_records_sizes_cum[0] + rank * num_threads;
        }
        else {
            sync_records_sizes_cum[rank] = sync_records_sizes_cum[rank - 1]
                + sync_records_sizes[rank - 1];
        }
    } /* end mpi_size loop */

    // allocate result memory on root rank
    if(mpi_rank == 0){
        m_result_scratch = (real *) mb_aligned_alloc(sizeof(real) *
            (sync_records_sizes[mpi_size - 1] + sync_records_sizes_cum[mpi_size - 1]));
    }

    compute_scratch_per_thread(scen);

    // allocate memory for sync records
    sync_records = new sync_record[sync_records_sizes[mpi_rank]];
    sync_records_thread = new sync_record*[num_threads];
    for (unsigned int tid = 0; tid < num_threads; tid++) {
        sync_records_thread_sizes[tid] += 1;
        sync_records_thread_sizes_cum[tid] += tid * 1;
        sync_records_thread[tid] = &sync_records[sync_records_thread_sizes_cum[tid]];
        sync_records_thread[tid][0].offset = 0;
    }
    sync_records_root = new sync_record[sync_records_sizes[mpi_size - 1]
                                    + sync_records_sizes_cum[mpi_size - 1]
                                    + num_threads * mpi_size];

    // convert sizes to byte sizes
    sync_records_sizes_byte = (int*)malloc(mpi_size * sizeof(int));
    sync_records_sizes_cum_byte = (int*)malloc(mpi_size * sizeof(int));
    for(int rank = 0; rank < mpi_size; rank++){
        sync_records_sizes_byte[rank] = sync_records_sizes[rank]
                                        * sizeof(sync_record);
        sync_records_sizes_cum_byte[rank] = sync_records_sizes_cum[rank]
                                        * sizeof(sync_record);
    }
    sync_records_all_thread_sizes = (int*)malloc(mpi_size * num_threads * sizeof(int));

#if USE_MPI
    // collect records sizes from all nodes at rank 0
    MPI_Gather(sync_records_thread_sizes, num_threads,
        MPI_INT, sync_records_all_thread_sizes, num_threads,
        MPI_INT, 0, mpi_comm);
#else
    for(int i = 0; i < num_threads; i++){
        sync_records_all_thread_sizes[i] = sync_records_thread_sizes[i];
    }
#endif
    if(mpi_rank == 0){
        sync_records_all_thread_sizes_cum = (int*)malloc(mpi_size * num_threads * sizeof(int));
        sync_records_all_thread_sizes_cum[0] = 0;
        for (unsigned int tid = 1; tid < mpi_size * num_threads; tid++) {
            sync_records_all_thread_sizes_cum[tid] = sync_records_all_thread_sizes_cum[tid - 1]
                                                    + sync_records_all_thread_sizes[tid - 1];
        }
    }
}

/*
 * Function, which computes the sizes of each thread-local scratch memory based
 * on the positions of the sources and the requested records.
 * input:
 *   std::shared_ptr<scenario> scen: the scenario of the setup
 */
template<unsigned int num_lvl, template<unsigned int> class density_algo>
void
solver_cpu_fdtd_red<num_lvl, density_algo>::compute_scratch_per_thread
(std::shared_ptr<scenario> scen)
{
    // calculate the memory per thread on that node
    uint64_t scratch_size_thread = 0;
    for(int tid = 0; tid < num_threads; tid++){
        unsigned int start_idx_thread = node_chunks_cumulated[mpi_rank]
                                        + thread_chunks_cumulated[tid];
        unsigned int end_idx_thread = start_idx_thread + thread_chunks[tid];

        for (const auto& rec : scen->get_records()) {
            copy_list_entry entry(rec, scen, scratch_size_thread);

            uint64_t position = entry.get_position();
            uint64_t cols = entry.get_cols();
            uint64_t size = entry.get_size();
            /* take imaginary part into account */
            if (rec->is_complex()) {
                size = size * 2;
            }
            double chunk_ratio = node_chunks[mpi_rank] * 1.0 / scen->get_num_gridpoints();
            uint64_t reduced_size = ceil(size * chunk_ratio);

            /* chunk_target is the relevant thread chunk for results */
            unsigned int chunk_target = thread_chunks[tid];
            if(tid == 0) {
                chunk_target = chunk_target - OL_node + OL_thread;
            }
            if(tid == num_threads - 1){
                 chunk_target = chunk_target - OL_node + OL_thread;
            }

            double chunk_ratio_thread = chunk_target * 1.0 / node_chunks[mpi_rank];
            uint64_t reduced_size_thread = ceil(reduced_size * chunk_ratio_thread);

            if(position < end_idx_thread && position >= start_idx_thread && cols == 1){
                scratch_size_thread += size;
                sync_records_thread_sizes[tid] += size;
            }
            else if (cols > 1){
                scratch_size_thread += reduced_size_thread;
                sync_records_thread_sizes[tid] += reduced_size_thread;
            }
        }
        if(tid > 0){
            sync_records_thread_sizes_cum[tid] = sync_records_thread_sizes_cum[tid - 1]
                                                + sync_records_thread_sizes[tid - 1];
        }
    }
}

/*
 * Function, which computes the load balancing, i.e. the distribution of the spatial
 * grid w.r.t. the given size and the position of the sources. Can be used both
 * for load balancing between nodes as well as for load balancing between threads.
 * input:
 *   unsigned int gridpoints: the number of gridpoints to distribute
 *   unsigned int base_idx: the index of the first gridpoint in the global space
 *   unsigned int size: number of partitions
 * output:
 *   unsigned int *local_chunks: pointer to the number of gridpoints per partition
 *   unsigned int *chunks_cumulated: pointer to the cumulated number of gridpoints
 */
template<unsigned int num_lvl, template<unsigned int> class density_algo>
void
solver_cpu_fdtd_red<num_lvl, density_algo>::distribute_chunks
(unsigned int gridpoints, unsigned int base_idx, unsigned int size,
    unsigned int *local_chunks, unsigned int *chunks_cumulated)
{
    unsigned int chunk_base = gridpoints / size;
    unsigned int chunk_rem = gridpoints % size;
    int start_idx, end_idx;
    for(unsigned int i = 0; i < size; i++){
        local_chunks[i] = chunk_base;
    }
    // distribute the remaining chunks equally among all partitions
    int i = size - 1;
    while(chunk_rem > 0){
        local_chunks[i]++;
        chunk_rem--;
        i--;
        i = (i < 0) ? size - 1 : i;
    }
    chunks_cumulated[0] = 0;
    for(unsigned int i = 1; i < size; i++){
        chunks_cumulated[i] = chunks_cumulated[i-1] + local_chunks[i-1];
    }
}


template<unsigned int num_lvl, template<unsigned int> class density_algo>
solver_cpu_fdtd_red<num_lvl, density_algo>::~solver_cpu_fdtd_red()
{
#pragma omp parallel
    {
        unsigned int tid = omp_get_thread_num();

        delete[] m_d[tid];
        mb_aligned_free(m_e[tid]);
        mb_aligned_free(m_h[tid]);
        mb_aligned_free(m_p[tid]);
        mb_aligned_free(m_fac_a[tid]);
        mb_aligned_free(m_fac_b[tid]);
        mb_aligned_free(m_fac_c[tid]);
        mb_aligned_free(m_gamma[tid]);
        mb_aligned_free(m_mat_indices[tid]);
        mb_aligned_free(m_source_data_threads[tid]);
    }

    if(mpi_rank == 0){
        mb_aligned_free(m_result_scratch);
    }
    delete[] m_source_data_threads;

    delete[] m_d;
    delete[] m_e;
    delete[] m_h;
    delete[] m_p;
    delete[] m_fac_a;
    delete[] m_fac_b;
    delete[] m_fac_c;
    delete[] m_gamma;
    delete[] m_mat_indices;

    delete[] sync_records;
    delete[] sync_records_root;
    delete[] sync_records_thread;

    free(sync_records_sizes_cum);
    free(sync_records_sizes);
#if USE_MPI
    free(requests);
    free(statuses);
#endif
    free(node_chunks);
    free(node_chunks_cumulated);
    free(thread_chunks);
    free(thread_chunks_cumulated);
    free(sync_records_thread_sizes);
    free(sync_records_thread_sizes_cum);
    free(sync_records_all_thread_sizes);
    if(mpi_rank == 0){
        free(sync_records_all_thread_sizes_cum);
    }
    free(sync_records_sizes_byte);
    free(sync_records_sizes_cum_byte);
}

/*template<unsigned int num_lvl, template<unsigned int> class density_algo>
const std::string&
solver_cpu_fdtd_red<num_lvl, density_algo>::get_name() const
{
    return m_name;
}*/

template<unsigned int num_lvl, template<unsigned int> class density_algo>
void
solver_cpu_fdtd_red<num_lvl, density_algo>::update_e
(uint64_t size, unsigned int border, real *e, real *h, real *p, real *fac_a,
 real *fac_b, real *gamma) const
{
#pragma omp simd aligned(e, h, p, fac_a, fac_b, gamma : ALIGN)
    for (int i = border; i < size - border - 1; i++) {
        e[i] = fac_a[i] * e[i] + fac_b[i] *
            (-gamma[i] * p[i] + m_dx_inv * (h[i + 1] - h[i]));
    }
}

template<unsigned int num_lvl, template<unsigned int> class density_algo>
void
solver_cpu_fdtd_red<num_lvl, density_algo>::update_h
(uint64_t size, unsigned int border, real *e, real *h, real *fac_c) const
{
#pragma omp simd aligned(e, h, fac_c : ALIGN)
    for (int i = border + 1; i < size - border; i++) {
        h[i] += fac_c[i] * (e[i] - e[i - 1]);
    }
}

template<unsigned int num_lvl, template<unsigned int> class density_algo>
void
solver_cpu_fdtd_red<num_lvl, density_algo>::apply_sources
(real *t_e, real *source_data, unsigned int num_sources,
 uint64_t time,
 unsigned int base_pos, uint64_t chunk, unsigned int tid) const
{
    for (unsigned int k = 0; k < num_sources; k++) {
        int at = m_sim_sources[k].x_idx - base_pos;
        if ((at > 0) && (at < chunk + 2 * OL_thread)) {
            real src = source_data[m_sim_sources[k].data_base_idx + time];
            if (m_sim_sources[k].type == source::type::hard_source) {
                t_e[at] = src;
            } else if (m_sim_sources[k].type == source::type::soft_source) {
                t_e[at] += src;
            }
        }
    }
}


template<unsigned int num_lvl, template<unsigned int> class density_algo>
void
solver_cpu_fdtd_red<num_lvl, density_algo>::run() const
{
    uint64_t num_gridpoints = m_scenario->get_num_gridpoints();
    uint64_t num_timesteps = m_scenario->get_num_timesteps();
    unsigned int num_sources = m_sim_sources.size();

    /* main loop */
    unsigned int subloop_ct_base;
    for (uint64_t n = 0; n <= num_timesteps/OL_node; n++) {
        /* handle loop remainder */
        subloop_ct_base = (n == num_timesteps/OL_node) ?
                        (num_timesteps % OL_node) : OL_node;

#if USE_MPI
        /* exchange data between nodes via MPI */
        if (mpi_size > 1){
            exchange_data_per_node();
        }
#endif

#pragma omp parallel
        {
            unsigned int tid = omp_get_thread_num();
            uint64_t chunk = thread_chunks[tid];
            uint64_t size = chunk + 2 * OL_thread;

            /* gather pointers */
            typedef typename density_algo<num_lvl>::density density_t;
            density_t *t_d;
            real *t_h, *t_e, *t_p;
            real *t_fac_a, *t_fac_b, *t_fac_c, *t_gamma;
            unsigned int *t_mat_indices;

            t_d = m_d[tid];
            t_e = m_e[tid];
            t_h = m_h[tid];
            t_p = m_p[tid];
            t_fac_a = m_fac_a[tid];
            t_fac_b = m_fac_b[tid];
            t_fac_c = m_fac_c[tid];
            t_gamma = m_gamma[tid];
            t_mat_indices = m_mat_indices[tid];

            __mb_assume_aligned(t_d);
            __mb_assume_aligned(t_e);
            __mb_assume_aligned(t_h);
            __mb_assume_aligned(t_p);
            __mb_assume_aligned(t_fac_a);
            __mb_assume_aligned(t_fac_b);
            __mb_assume_aligned(t_fac_c);
            __mb_assume_aligned(t_gamma);
            __mb_assume_aligned(t_mat_indices);

            /* gather previous and next pointers from other threads */
            density_t *n_d, *p_d;
            real *n_h, *n_e;
            real *p_h, *p_e;

            __mb_assume_aligned(p_d);
            __mb_assume_aligned(p_e);
            __mb_assume_aligned(p_h);

            __mb_assume_aligned(n_d);
            __mb_assume_aligned(n_e);
            __mb_assume_aligned(n_h);

            if (tid > 0) {
                p_d = m_d[tid - 1];
                p_e = m_e[tid - 1];
                p_h = m_h[tid - 1];
            }

            if (tid < num_threads - 1) {
                n_d = m_d[tid + 1];
                n_e = m_e[tid + 1];
                n_h = m_h[tid + 1];
            }

            unsigned int border, thread_subloop_ct;
            unsigned int subloop_ct = subloop_ct_base/OL_thread;

            /* subloop */
            for (uint64_t m = 0; m <= subloop_ct; m++) {
                /* handle loop remainder */
                thread_subloop_ct = (m == subloop_ct) ?
                                    subloop_ct_base % OL_thread : OL_thread;

                /* sync before communication*/
#pragma omp barrier
                /* exchange data */
                if (tid > 0) {
#pragma ivdep
                    for (unsigned int i = 0; i < OL_thread; i++) {
                        t_d[i] = p_d[thread_chunks[tid - 1] + i];
                        t_e[i] = p_e[thread_chunks[tid - 1] + i];
                        t_h[i] = p_h[thread_chunks[tid - 1] + i];
                    }
                }

                if (tid < num_threads - 1) {
#pragma ivdep
                    for (unsigned int i = 0; i < OL_thread; i++) {
                        t_d[OL_thread + thread_chunks[tid] + i] = n_d[OL_thread + i];
                        t_e[OL_thread + thread_chunks[tid] + i] = n_e[OL_thread + i];
                        t_h[OL_thread + thread_chunks[tid] + i] = n_h[OL_thread + i];
                    }
                }

                /* sync after thread-local communication */
#pragma omp barrier

                /* thread - sub-loop */
                for (unsigned int j = 0; j < thread_subloop_ct; j++) {
                    /* align border to vector length */
                    border = j - (j % VEC);

                    for (int i = j; i < size - j - 1; i++) {
                        int mat_idx = t_mat_indices[i];

                        /* update density matrix */
                        density_algo<num_lvl>::update(m_sim_consts_qm[mat_idx],
                                                      t_d[i], t_e[i], &t_p[i]);
                    }

                    /* update electric field */
                    update_e(size, border, t_e, t_h, t_p, t_fac_a, t_fac_b,
                             t_gamma);

                    /* apply sources */
                    apply_sources(t_e, m_source_data_threads[tid], num_sources,
                                    n * OL_node + m * OL_thread + j,
                                    node_chunks_cumulated[mpi_rank] - OL_node +
                                    thread_chunks_cumulated[tid],
                                    chunk, tid);

                    /* update magnetic field */
                    update_h(size, border, t_e, t_h, t_fac_c);

                    /* apply field boundary condition */
                    if (mpi_rank == 0 && tid == 0) {
                        t_h[OL_node] = 0;
                    }
                    if (mpi_rank == mpi_size - 1 && tid == num_threads - 1) {
                        t_h[size - OL_node] = 0;
                    }

                    /* save results to scratchpad */
                    save_records_per_node(n * OL_node + m * OL_thread + j, tid,
                                            size, t_e, t_h, t_d);

                } /* end thread subloop */

            } /* end subloop */

        } /* end openmp region */

    } /* end main loop */

#if USE_MPI
    // collect records from all nodes at rank 0
    MPI_Gatherv(sync_records, sync_records_sizes_byte[mpi_rank],
        MPI_BYTE, sync_records_root, sync_records_sizes_byte,
        sync_records_sizes_cum_byte, MPI_BYTE, 0, mpi_comm);

    // copy received data to scratch memory
    if(mpi_rank == 0){
        size_t index = 0, offset = 0;
        for(size_t j = 0; j < mpi_size; j++){
            for(size_t k = 0; k < num_threads; k++){
                offset = sync_records_all_thread_sizes_cum[j * num_threads + k];
                for (size_t i = 0; i < sync_records_all_thread_sizes[j *
                                                    num_threads + k] - 1; i++){
                    index = offset + i + 1;
                    m_result_scratch[sync_records_root[index].offset] =
                                        sync_records_root[index].value;
                }
            }
        }
    }
#else
    size_t index = 0, offset = 0;
    for(size_t k = 0; k < num_threads; k++){
        offset = sync_records_all_thread_sizes_cum[k];
        for (size_t i = 0; i < sync_records[offset].offset; i++) {
            index = offset + i + 1;
            m_result_scratch[sync_records[index].offset] =
                                sync_records[index].value;
        }
    }
#endif

    if(mpi_rank == 0){
        /* bulk copy results into result classes */
        for (const auto& cle : m_copy_list) {
            real *dr = m_result_scratch + cle.get_offset_scratch_real(0, 0);
            std::copy(dr, dr + cle.get_size(), cle.get_result_real(0, 0));
            if (cle.is_complex()) {
                real *di = m_result_scratch + cle.get_offset_scratch_imag(0, 0);
                std::copy(di, di + cle.get_size(), cle.get_result_imag(0, 0));
            }
        }
    }
}


/* Function, which exchanges the field values of each node with its neighbors.
 * Using non-blocking point-to-point MPI communication
 */
template<unsigned int num_lvl, template<unsigned int> class density_algo>
void
solver_cpu_fdtd_red<num_lvl, density_algo>::exchange_data_per_node() const {
#if USE_MPI
    unsigned int num_threads = omp_get_max_threads();
    sync_buf<OL_node> send_buf_r;
    sync_buf<OL_node> send_buf_l;
    sync_buf<OL_node> recv_buf_r;
    sync_buf<OL_node> recv_buf_l;

    if ((mpi_rank + 1) < mpi_size) {
        // send data to the right neighbor
#pragma ivdep
        for (unsigned int i = 0; i < OL_node; i++) {
            send_buf_r.d[i] = m_d[num_threads - 1][thread_chunks[num_threads - 1] +
                                                    2 * OL_thread - 2 * OL_node + i];
            send_buf_r.e[i] = m_e[num_threads - 1][thread_chunks[num_threads - 1] +
                                                    2 * OL_thread - 2 * OL_node + i];
            send_buf_r.h[i] = m_h[num_threads - 1][thread_chunks[num_threads - 1] +
                                                    2 * OL_thread - 2 * OL_node + i];
        }
        MPI_Isend(&send_buf_r, sizeof(sync_buf<OL_node>), MPI_BYTE, mpi_rank + 1,
                    0, mpi_comm, &requests[mpi_rank * 4]);
        // receive data from the right neighbor
        MPI_Irecv(&recv_buf_r, sizeof(sync_buf<OL_node>), MPI_BYTE, mpi_rank + 1,
                0, mpi_comm, &requests[mpi_rank * 4 + 1]);
    }
    if (mpi_rank > 0) {
        // send data to the left neighbor
#pragma ivdep
        for (unsigned int i = 0; i < OL_node; i++) {
            send_buf_l.d[i] = m_d[0][OL_node + i];
            send_buf_l.e[i] = m_e[0][OL_node + i];
            send_buf_l.h[i] = m_h[0][OL_node + i];
        }
        MPI_Isend(&send_buf_l, sizeof(sync_buf<OL_node>), MPI_BYTE, mpi_rank - 1,
            0, mpi_comm, &requests[(mpi_rank - 1) * 4 + 2]);
        // receive data from the left neighbor
        MPI_Irecv(&recv_buf_l, sizeof(sync_buf<OL_node>), MPI_BYTE, mpi_rank - 1,
            0, mpi_comm, &requests[(mpi_rank - 1) * 4 + 3]);
    }

   // TODO: implement overlap of communication and computation here

    // wait for communication with left neighbor to be finished
    if(mpi_rank > 0){
        MPI_Waitall(2, &requests[(mpi_rank - 1) * 4 + 2],
                   &statuses[(mpi_rank - 1) * 4 + 2]);
#pragma ivdep
        for (unsigned int i = 0; i < OL_node; i++) {
            m_d[0][i] = recv_buf_l.d[i];
            m_e[0][i] = recv_buf_l.e[i];
            m_h[0][i] = recv_buf_l.h[i];
        }
    }
    // wait for communication with right neighbor to be finished
    if((mpi_rank + 1) < mpi_size){
        MPI_Waitall(2, &requests[mpi_rank * 4],
           &statuses[mpi_rank * 4]);
#pragma ivdep
        for (unsigned int i = 0; i < OL_node; i++) {
            m_d[num_threads - 1][thread_chunks[num_threads - 1] +
                                 2 * OL_thread - OL_node + i] = recv_buf_r.d[i];
            m_e[num_threads - 1][thread_chunks[num_threads - 1] +
                                 2 * OL_thread - OL_node + i] = recv_buf_r.e[i];
            m_h[num_threads - 1][thread_chunks[num_threads - 1] +
                                 2 * OL_thread - OL_node + i] = recv_buf_r.h[i];
        }
    }
#endif // USE_MPI
}


/*
 * Function, which tests if the computed values at a timestep have to be recorded.
 * If true, it iterates through the whole grid and writes the records to a buffer.
 * input:
 *   unsigned int time: the number of the timestep
 *   unsigned int tid: the thread ID of the OpenMP thread
 *   uint64_t size: the size of the grid of the OpenMP thread
 *   real *t_e: pointer to the electrical field values
 *   real *t_h: pointer to the magnetival field values
 *   density_algo<num_lvl>::density *t_d: pointer to the density values
 */
template<unsigned int num_lvl, template<unsigned int> class density_algo>
inline void
solver_cpu_fdtd_red<num_lvl, density_algo>::save_records_per_node(
    unsigned int time, unsigned int tid, uint64_t size, real *t_e, real *t_h,
    typename density_algo<num_lvl>::density *t_d) const {

    unsigned int num_copy = m_copy_list.size();
    uint64_t chunk = thread_chunks[tid];
    uint64_t pos, cols, ridx, cidx;
    int64_t base_idx, off_r, loop_start, loop_end, idx;
    record::type t;
    /* iterate through all the previously specified records */
    for (int k = 0; k < num_copy; k++) {
        if (m_copy_list[k].hasto_record(time)) {
            pos = m_copy_list[k].get_position();
            cols = m_copy_list[k].get_cols();
            ridx = m_copy_list[k].get_row_idx();
            cidx = m_copy_list[k].get_col_idx();
            record::type t = m_copy_list[k].get_type();

            base_idx = (int64_t)node_chunks_cumulated[mpi_rank] +
                        (int64_t)thread_chunks_cumulated[tid] - OL_node;
            off_r = m_copy_list[k].get_offset_scratch_real
                    (time, base_idx - (int64_t)pos);

            /* set the loop boundaries correctly */
            loop_start = OL_thread;
            loop_end = chunk + OL_thread;
            if(tid == 0) {
                loop_start = OL_node;
            }
            if(tid == num_threads - 1) {
                loop_end = size - OL_node;
            }

            /* iterate through the spatial grid */
            for (uint64_t i = loop_start; i < loop_end; i++) {
                idx = base_idx + i;
                if ((idx >= pos) && (idx < pos + cols)) {
                    if (t == record::type::electric) {
                        write_sync_record(off_r + i, t_e[i], tid);
                    } else if (t == record::type::magnetic) {
                        write_sync_record(off_r + i, t_h[i], tid);
                    } else if (t == record::type::inversion) {
                        write_sync_record(off_r + i,
                            density_algo<num_lvl>::calc_inversion(t_d[i]), tid);

                    } else if (t == record::type::density) {

                        /* right now only populations */
                        if (ridx == cidx) {
                            write_sync_record(off_r + i,
                                density_algo<num_lvl>::calc_population(t_d[i], ridx), tid);
                        }
                    }
                }
            }
        } /* end if has to record */
    } /* end num_copy loop */
}


/*
 * Function, which writes the records to a buffer on a node-level.
 * using omp critical due to data race for the recorder_counter.
 * input:
 *   size_t offset: position of the record in the global grid
 *   real value: the value of the record
 */
template<unsigned int num_lvl, template<unsigned int> class density_algo>
inline void
solver_cpu_fdtd_red<num_lvl, density_algo>::write_sync_record(
    size_t offset, real value, unsigned int thread_id) const {
    size_t recorder_counter = sync_records_thread[thread_id][0].offset;

    if (recorder_counter >= sync_records_thread_sizes[thread_id] - 1) {
        throw std::invalid_argument("Too many results");
    } else {
        sync_records_thread[thread_id][1 + recorder_counter].offset = offset;
        sync_records_thread[thread_id][1 + recorder_counter].value = value;
        sync_records_thread[thread_id][0].offset += 1;
    }
}
}
#endif
