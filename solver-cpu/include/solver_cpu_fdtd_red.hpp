/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#ifndef MBSOLVE_SOLVER_CPU_FDTD_RED_H
#define MBSOLVE_SOLVER_CPU_FDTD_RED_H

#if !defined(USE_MPI)
# error "You should specify USE_MPI=0 or USE_MPI=1 on the compile line"
#endif

#if USE_MPI
#include <mpi.h>
#endif
#include <cw_solver.hpp>
#include <internal/common_fdtd.hpp>
#include <internal/copy_list_entry.hpp>
#include <internal/algo_lindblad_cvr_rodr.hpp>
#include <internal/algo_lindblad_rvr_action.hpp>
#include <internal/algo_lindblad_rvr_newton.hpp>

namespace mbsolve {

/**
 * MPI+OpenMP solver for c-lvl systems using the FDTD method (optimized version
 * using redundant calculations).
 * The number of levels c can be chosen arbitrarily, but must be known at
 * compile time, as well as the algorithm for updating the density matrix.
 * This class is derived from \ref cw_solver.
 * \ingroup MBSOLVE_SOLVER_HYBRID
 */
template<unsigned int num_lvl, template<unsigned int> class density_algo>
class solver_cpu_fdtd_red : public cw_solver {

    /**
     * Data structure for exchanging halo data between nodes.
     * The length OL of each array must be known at compile-time.
     */
    template<unsigned int OL>
    struct sync_buf {
        real e[OL];
        real h[OL];
        typename density_algo<num_lvl>::density d[OL];
    };

    /**
     * Data structure to store the position and value of a grid point
     * thread-locally.
     */
    struct sync_record {
        int64_t offset;
        real value;
    };



public:
    /**
     * Constructs the solver with a given setup with calling the constructor
     * of the parent class.
     *
     * \param [in] dev  Specify the \ref device to be simulated.
     * \param [in] scen Specify the \ref scenario.
     * \param [in] init_method Specify the initialization method.
     */
    solver_cpu_fdtd_red(std::shared_ptr<device> dev,
                           std::shared_ptr<scenario> scen,
                        std::string init_method);

#if USE_MPI
    /**
     * Constructs the solver with a given setup with calling the constructor
     * of the parent class.
     *
     * \param [in] dev  Specify the \ref device to be simulated.
     * \param [in] scen Specify the \ref scenario.
     * \param [in] init_method Specify the initialization method.
     * \param [in] comm MPI communicator.
     */
    solver_cpu_fdtd_red(std::shared_ptr<device> dev,
                           std::shared_ptr<scenario> scen,
                        std::string init_method, MPI_Comm comm);
#endif
    /**
     * Destructs the solver and releases all allocated memory.
     */
    ~solver_cpu_fdtd_red();

    /**
     * Initializes all needed data structures and allocates thread-local memory.
     * A MPI Communicator should be set before calling this function.
     */
    void init();

    /**
     * Runs the solver.
     */
    void run() const;


private:
    /**
     * Computes and allocates the scratch memory for each node.
     * Depending on the \ref scenario and the position of predefined \ref records,
     * this method allocates the appropriate size of the scratch memory, where
     * each node stores its computed \ref records. It also computes the thread-local
     * scratch memory by calling compute_scratch_per_thread().
     *
     * \param [in] scen The \scenario.
     */
    void compute_scratch_per_node(std::shared_ptr<scenario> scen);

    /**
     * Computes and allocates the scratch memory for each thread.
     * Depending on the \ref scenario and the position of predefined \ref records,
     * this method allocates the appropriate size of the scratch memory, where
     * each thread stores its computed \ref records.
     *
     * \param [in] scen The \scenario.
     */
    void compute_scratch_per_thread(std::shared_ptr<scenario> scen);

    /**
     * Distributes the spatial domain among the given compute resources.
     * Depending on the position of the \ref source of the \scenario, this method
     * can divide the gridpoints equally or non-equally among the resources
     * (threads or nodes).
     *
     * \param [in] gridpoints The number of gridpoints, which should be distributed.
     * \param [in] base_idx The global index of the leftmost gridpoint.
     * \param [in] size The size of the compute resources (number of threads/nodes).
     * \param [out] local_chunks Array, which stores the chunk sizes of all
     *              compute resources.
     * \param [out] chunks_cumulated Array, which stores the cumulated sizes
                    of all compute resources.
     */
    void distribute_chunks(unsigned int gridpoints, unsigned int base_idx,
        unsigned int size, unsigned int *local_chunks, unsigned int *chunks_cumulated);

    /**
     * Updates the electric field array.
     *
     * \param [in] size The number of gridpoints, which will be updated.
     * \param [in] border The number of gridpoints at the border, which will
     *             be excluded from the update.
     * \param [in] e Pointer to the electric field values.
     * \param [in] h Pointer to the magnetic field values.
     * \param [in] p Pointer to the polarization field values.
     * \param [in] fac_a Pointer tot the simulation constants a.
     * \param [in] fac_b Pointer tot the simulation constants b.
     * \param [in] real Pointer tot the gamma values.
     */
    void update_e(uint64_t size, unsigned int border, real *e, real *h,
                  real *p, real *fac_a, real *fac_b, real *gamma) const;

    /**
     * Updates the magnetic field array.
     *
     * \param [in] size The number of gridpoints, which will be updated.
     * \param [in] border The number of gridpoints at the border, which will
     *             be excluded from the update.
     * \param [in] e Pointer to the electric field values.
     * \param [in] h Pointer to the magnetic field values.
     * \param [in] fac_c Pointer tot the simulation constants c.
     */
    void update_h(uint64_t size, unsigned int border, real *e, real *h,
                  real *fac_c) const;

    /**
     * Applies predefined electric sources.
     *
     * \param [in] t_e Pointer to the electric field values.
     * \param [in] source_data Pointer to the precomputed data of the sources.
     * \param [in] num_sources Number of available sources in the setup.
     * \param [in] time Current timestep value.
     * \param [in] base_pos The global position of the leftmost gridpoint
     *             of the thread.
     * \param [in] chunk The number of gridpoints of the calling thread.
     * \param [in] tid The thread id of the calling Thread.
     */
    void apply_sources(real *t_e, real *source_data, unsigned int num_sources,
                       uint64_t time, unsigned int base_pos, uint64_t chunk,
                       unsigned int tid) const;

    /**
     * Exchanges halo values with neighbouring nodes.
     * This method gets called for a node count > 1 and uses MPI non-blocking
     * point-to-point communication and synchronizes all processes using
     * MPI_Waitall().
     */
    void exchange_data_per_node() const;

    /**
     * Writes the computed values to the scratch memory per node.
     * The scratch memory is a consecutive space in memory and each thread
     * writes to its own partition of this space. Therefore, the method
     * write_sync_record() gets called. Before writing all the values, this
     * function checks, if the values have to be written and if so, it only
     * writes the requested ones.
     *
     * \param [in] time Current timestep value.
     * \param [in] tid The thread id of the calling thread.
     * \param [in] size The number of gridpoints of the calling thread.
     * \param [in] t_e Pointer to the electric field values.
     * \param [in] t_h Pointer to the magnetic field values.
     * \param [in] t_d Pointer to the density matrix values.
     */
    void save_records_per_node(unsigned int time, unsigned int tid,
        uint64_t size, real *t_e, real *t_h,
        typename density_algo<num_lvl>::density *t_d) const;

    /**
     * Writes the computed values to the thread-local scratch memory.
     *
     * \param [in] offset The offset of the value w.r.t. global domain.
     * \param [in] value The previously computed value.
     * \param [in] tid The thread id of the calling Thread.
     */
    void write_sync_record(uint64_t offset, real value, unsigned int tid) const;

    int mpi_size; /**< Number of MPI processes. */
    int mpi_rank; /**< Rank of the MPI process. */

#if USE_MPI
    MPI_Comm mpi_comm = MPI_COMM_NULL; /**< MPI Communicator. */
    MPI_Request *requests; /**< Array of MPI Requests for exchanging halo data. */
    MPI_Status *statuses; /**< Array of MPI Statuses for exchanging halo data. */
#endif

    /** Array storing the number of records received from each node. */
    int *sync_records_sizes;
    /** Array storing the cumulated number of of records per node. */
    int *sync_records_sizes_cum;

    /** Array storing the number of bytes received from each node. */
    int *sync_records_sizes_byte;
    /** Array storing the cumulated number of bytes per node. */
    int *sync_records_sizes_cum_byte;

    /** Array counting the number of records per thread on this node. */
    int *sync_records_thread_sizes;
    /** Array counting the cumulated number of records per thread on this node. */
    int *sync_records_thread_sizes_cum;

    /** Array storing the sizes of all threads on all nodes (only valid at root). */
    int *sync_records_all_thread_sizes;
    /** Array storing the cumulated sizes of all threads on all nodes (only valid at root). */
    int *sync_records_all_thread_sizes_cum;

    /** Array storing the data, that has all the records .*/
    sync_record *sync_records;
    /**
     * Array of pointers per thread pointing to the memory
     * where each thread will store its records.
     */
    sync_record **sync_records_thread;
    /** Array for receiving the records from all the nodes. */
    sync_record *sync_records_root;

    /** Array with the number of chunks per node. */
    unsigned int *node_chunks;
    /** Array with the cumulated number of chunks per node. */
    unsigned int *node_chunks_cumulated;

    /** Array with the number of chunks per thread. */
    unsigned int *thread_chunks;
    /** Array with the cumulated number of chunks per thread. */
    unsigned int *thread_chunks_cumulated;

    unsigned int num_threads; /**< Number of threads per node. */

    uint64_t chunk_base_node; /**< Base chunks per node. */
    uint64_t chunk_rem_node; /**< Chunk Remainder of all nodes. */

    uint64_t chunk_base; /**< Base chunk per thread. */
    uint64_t chunk_rem; /**< Chunk Remainder of all threads. */

    /**
     * Position-dependent density matrix.
     */
    typename density_algo<num_lvl>::density **m_d;

    real **m_e; /**< Position-dependent electric field values, thread-local.*/
    real **m_h; /**< Position-dependent magnetic field values, thread-local.*/
    real **m_p; /**< Position-dependent polarization field values, thread-local.*/

    real **m_fac_a; /**< Prefactor a values, thread-local.*/
    real **m_fac_b; /**< Prefactor b values, thread-local.*/
    real **m_fac_c; /**< Prefactor c values, thread-local.*/
    real **m_gamma; /**< Overlap factor values, thread-local.*/

    real m_dx_inv;

    real *m_result_scratch; /**< Scratch memory for results. Only valid at root. */
    uint64_t m_scratch_size; /**< Size of sratch memory for results. */

    real **m_source_data_threads; /**< Precomputed values of source data, thread-local. */

    unsigned int **m_mat_indices; /**< Material indices, thread-local. */

    /**
     * Simulation constants.
     */
    typedef typename density_algo<num_lvl>::sim_constants qm_consts;
    /**
     * Allocator for Eigen Library.
     */
    typedef typename density_algo<num_lvl>::allocator qm_allocator;
    /**
     * Quantum-mechanical simulation constants.
     */
    std::vector<qm_consts, qm_allocator> m_sim_consts_qm;

    std::vector<sim_source> m_sim_sources; /**< Simulation sources. */
    /**
     * List with entries, storing information about records.
     */
    std::vector<copy_list_entry> m_copy_list;

    /**
     * Tuning parameter: OpenMP overlap.
     */
    const unsigned int OL_thread;

    const std::string m_init_method; /**< Name of the initialization method. */
};

}

#endif
