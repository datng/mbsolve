#!/bin/bash

# parse arguments
for i in "$@"
do
case $i in
    -jc=*|--job-count=*) # specify number of parallel jobs
    JOB_COUNT="${i#*=}"
    ;;
esac
done



# check if job count is valid
case $JOB_COUNT in
    '')
    JOB_COUNT=1
    echo "Job count not specified. Set to $JOB_COUNT."
    echo
    ;;
    *[!0-9]*)
    JOB_COUNT=1
    echo "Invalid job count specified. Set to $JOB_COUNT."
    echo
    ;;
esac

if [ $JOB_COUNT -gt $(nproc) ]; then
    JOB_COUNT=$(nproc)
    echo "The specified job count exceeds the number of available processors. Set to $JOB_COUNT"
    echo
fi



cd unit-test

if [ -d "build" ]; then
    rm -r build
fi
mkdir build
cd build

# only cmake 2.8 is available on CentOS Linux 7
# cmake 3.13 can be installed from the EPEL repository, but it is called cmake3
if [[ $(cat /etc/os-release | grep "CentOS Linux 7") != "" ]]; then
    # only gcc4.8 can be installed in the default repository.
    # This has problem with linking when using explicit template class initialization.
    # The official solution for this is to use a newer developer toolset from the scl repository.
    # The minimum available version that can compile mbsolve is 4, using gcc 5.3.1
    BUILD_ENV_PREFIX="scl enable devtoolset-4 --"
    $BUILD_ENV_PREFIX cmake3 .. || { exit 1; }
else
    cmake .. || { exit 1; }
fi

$BUILD_ENV_PREFIX make -j$JOB_COUNT || {
    exit 1;
}

cd ..
cd ..
