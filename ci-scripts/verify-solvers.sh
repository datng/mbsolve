#!/bin/bash

# parse arguments
for i in "$@"
do
case $i in
    -jc=*|--job-count=*) # specify number of parallel jobs
    JOB_COUNT="${i#*=}"
    ;;
    -f|--force-rebuild=*) # rebuild the build directories
    FORCE_REBUILD=TRUE
    ;;
esac
done



SOLVER_TYPES=(
"hybrid"
"reference"
)



SETUPS=(
"-i zero -m cpu-fdtd-red-2lvl-cvr-rodr -d ziolkowski1995 -w hdf5 -g 8192 -e 300e-15"
"-i zero -m cpu-fdtd-red-2lvl-cvr-rodr -d ziolkowski1995 -w hdf5 -g 16384 -e 300e-15"
"-i zero -m cpu-fdtd-red-2lvl-cvr-rodr -d ziolkowski1995 -w hdf5 -g 32768 -e 300e-15"
)



# prepare a verification folder
if [ -d verification ]; then
    rm -r verification/
fi
mkdir verification

for i in "${SOLVER_TYPES[@]}"; do # build and copy mbsolve-tool from each solver's build directory
    if [ ! -d "build-$i" ]; then
        WILL_BUILD=TRUE
    elif [[ $FORCE_REBUILD == TRUE ]]; then
        rm -r "build-$i"
        WILL_BUILD=TRUE
    fi

    if [[ $WILL_BUILD == TRUE ]]; then
        ./ci-scripts/build.sh --solver-type=$i -jc=$JOB_COUNT || {
            echo "Could not build $i solver."
            exit 1
        }
    fi
    cp build-$i/mbsolve-tool/mbsolve-tool verification/mbsolve-tool-$i || {
        echo "Could not copy the $i solver tool to the verification directory."
        exit 1
    }
done

cd verification



echo "Generating verification results..."
for solver in "${SOLVER_TYPES[@]}"; do
    echo $solver
    for setup in "${SETUPS[@]}"; do
        echo $setup
        output="$solver $setup.hdf5"

        ./mbsolve-tool-$solver $setup $ -o "$output" || {
            echo "Could not generate $solver result for setup $setup."
            exit 1
        }
    done
done
echo


# verify results
echo "Verifying results..."
for solver in "${SOLVER_TYPES[@]}"; do
    case $solver in "reference")
        continue
    esac
    echo "Verifying $solver solver..."
    for setup in "${SETUPS[@]}"; do
        h5diff "reference $setup.hdf5" "$solver $setup.hdf5" > "diff $solver $setup.txt" || {
            echo "Could not compare setup $setup of solver $solver."
            exit 1
        }
        difference=$(stat -c%s "diff $solver $setup.txt" || {
            exit 1
        }
        )
        if [ $difference -gt 0 ]; then
            echo "File $FILE_1 contains wrong results or is not comparable to its reference."
            FAILING=TRUE
        fi
    done
done
echo


# result
echo "Verification complete."
if [ -n "$FAILING" ]; then
    echo "Some tests failed. Please check the output."
    exit 1
else
    echo "All tests succeeded."
fi



# go back to the root directory
cd ..
