#!/bin/bash

if [ ! -d "build" ]; then
    mkdir build    
fi

cd build

# only cmake 2.8 is available on CentOS Linux 7
# cmake 3.13 can be installed from the EPEL repository, but it is called cmake3
if [[ $(cat /etc/os-release | grep "CentOS Linux 7") != "" ]]; then
    cmake3 -DWITH_DOXYGEN=ON .. || {
        exit 1;
    }
else
    cmake -DWITH_DOXYGEN=ON .. || {
        exit 1;
    }
fi

make doc || {
    exit 1;
}

python3 -m coverxygen --xml-dir documentation/xml --src-dir .. --output documentation-coverage.txt || {
    exit 1;
}

genhtml --no-function-coverage --no-branch-coverage documentation-coverage.txt -o documentation-coverage || {
    exit 1;
}
