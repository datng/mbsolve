#!/bin/sh
# check runner system
if [[ $(cat /etc/os-release | grep "CentOS Linux 7") != "" ]]; then
    cmake3 --version || { echo "Could not find cmake3."; exit 1; }
    scl enable devtoolset-4 -- g++ --version || { echo "Could not find g++."; exit 1; }
else
    cmake --version || { echo "Could not find cmake."; exit 1; }
    g++ --version || { echo "Could not find g++."; exit 1; }
fi
doxygen -v || { echo "Could not find doxygen."; exit 1; }
lcov -v || { echo "Could not find lcov."; exit 1; }
pip3 -V || { echo "Could not find pip3."; exit 1; }
uname -a || { echo "Could not find uname."; exit 1; }
unzip -v || { echo "Could not find unzip."; exit 1; }
wget --version || { echo "Could not find wget."; exit 1; }



# install external dependencies
if [ ! -d external ]; then
    mkdir external
fi
cd external

# googletest
if [ ! -f release-1.8.1.zip ]; then
    wget https://github.com/google/googletest/archive/release-1.8.1.zip -O googletest-1.8.1.zip
fi
if [ -d googletest-release-1.8.1 ]; then
    rm -rf googletest-release-1.8.1
fi
if [ -d googletest ]; then
    rm -rf googletest
fi
unzip googletest-1.8.1.zip
rm googletest-1.8.1.zip
mv googletest-release-1.8.1 googletest

