# Purpose
- Each script in this directory corresponds to a stage in the continuous integration pipeline.
- They are written in such a way that they can be run multiple times on the same machine.

# Usage
- The scripts must be run from the repository's root directory, for example: `./ci-scripts/build.sh`

## Common options
- `-m, --multithreaded`: uses all CPU cores while compiling. This is important in stages where one binary is generated from many source files, for example, libsolver-cpu can utilize up to 12 cores with 12 explicit template class instantiations spread across 12 different files. Note that this will rack up RAM usage as well and might create a bottleneck. Make sure the system is not memory-limited.

## build.sh
- This script builds mbsolve-lib and mbsolve-tools
- Options:
  - [`-m, --multithreaded`](#Common-options)
  - `-ref, --reference`: compiles the reference CPU implementation. To be used when running verification tests.

## verify-solver.sh
- This script builds the solver tool and compares its results with the reference solver, which it also builds.
- Options:
  - [`-m, --multithreaded`](#Common-options)
