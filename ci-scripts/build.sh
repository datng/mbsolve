#!/bin/bash

# parse arguments
for i in "$@"
do
case $i in
    -jc=*|--job-count=*) # specify number of parallel jobs
    JOB_COUNT="${i#*=}"
    ;;
    --solver-type=*) # specify solvers that can be built. Default to reference.
    SOLVER_TYPE="${i#*=}"
    ;;
esac
done



# check if job count is valid
case $JOB_COUNT in
    '')
    JOB_COUNT=1
    echo "Job count not specified. Set to $JOB_COUNT."
    ;;
    *[!0-9]*)
    JOB_COUNT=1
    echo "Invalid job count specified. Set to $JOB_COUNT."
    ;;
esac

if [ $JOB_COUNT -gt $(nproc) ]; then
    JOB_COUNT=$(nproc)
    echo "The specified job count exceeds the number of available processors. Set to $JOB_COUNT"
fi



# differentiate if building the reference solver or others
if [[ ${SOLVER_TYPE^^} == "HYBRID" ]]; then
    BUILD_DIR="build-hybrid"
    BUILD_OPTIONS='-DWITH_OPENMP=OFF -DWITH_MPI=OFF -DWITH_HYBRID=ON'
else
    SOLVER_TYPE="reference"
    BUILD_DIR="build-reference"
    BUILD_OPTIONS='-DWITH_OPENMP=ON -DWITH_MPI=OFF -DWITH_HYBRID=OFF'
fi
echo "Solver type: $SOLVER_TYPE"
echo "CMake build options: $BUILD_OPTIONS"
echo



if [ ! -d "$BUILD_DIR" ]; then
    mkdir "$BUILD_DIR"
fi

cd "$BUILD_DIR"

# only cmake 2.8 is available on CentOS Linux 7
# cmake 3.13 can be installed from the EPEL repository, but it is called cmake3
echo "Generating makefiles..."
if [[ $(cat /etc/os-release | grep "CentOS Linux 7") != "" ]]; then
    # only gcc4.8 can be installed in the default repository.
    # This has problem with linking when using explicit template class initialization.
    # The official solution for this is to use a newer developer toolset from the scl repository.
    # The minimum available version that can compile mbsolve is 4, using gcc 5.3.1
    BUILD_ENV_PREFIX="scl enable devtoolset-4 --"

    # cmake3 on CentOS Linux 7 does not detect openMPI installations by default.
    BUILD_OPTIONS_MPI_C="-DMPI_C_COMPILER=/usr/lib64/openmpi/bin/mpicc"
    BUILD_OPTIONS_MPI_CXX="-DMPI_CXX_COMPILER=/usr/lib64/openmpi/bin/mpicxx"

    $BUILD_ENV_PREFIX cmake3 '$BUILD_OPTIONS' $BUILD_OPTIONS_MPI_C $BUILD_OPTIONS_MPI_CXX .. || { exit 1; }
else
    cmake '$BUILD_OPTIONS' .. || { exit 1; }
fi
echo

echo "Building..."
$BUILD_ENV_PREFIX make -j$JOB_COUNT || {
    exit 1
}
echo "Done."
echo

cd ..
