#!/bin/bash

# parse arguments
for i in "$@"
do
case $i in
    -i|--in-place)
    IN_PLACE=TRUE
    ;;
esac
done

clang-format --version || {
    echo "Could not find clang-format."
    exit 1
}
echo

FILE_COUNT=0
REPLACEMENT_COUNT=0
for filename in $(find -name '*.[cht]pp'); do
    case $filename in
        ./build*) # ignore files in the build folder
            continue
        ;;
    esac

    count=$(clang-format --output-replacements-xml $filename | grep -Poc "<replacement offset")
    if [[ $count -eq 0 ]]; then
        continue
    fi

    FILE_COUNT=$(($FILE_COUNT+1))
    REPLACEMENT_COUNT=$(($REPLACEMENT_COUNT+$count))

    if [[ ${IN_PLACE^^} == "TRUE" ]]; then
        clang-format -i $filename
    else
        echo -e "File $filename requires changes in $count places."
    fi
done

if [[ $REPLACEMENT_COUNT -eq 0 ]]; then
    echo "All files formatted correctly."
    exit 0
fi

if [[ ${IN_PLACE^^} != "TRUE" ]]; then
    echo "Clang-format suggests $REPLACEMENT_COUNT changes in $FILE_COUNT files."
    echo "Run this script again with the argument -i to apply the changes."
    exit 1
else
    echo "Clang-format made $REPLACEMENT_COUNT changes in $FILE_COUNT files."
fi
