clang-tidy --version || {
    echo "Could not find clang-tidy."
    exit 1
}
echo

ANALYSIS_FILE="static-analysis.txt"
if [ -f "$ANALYSIS_FILE" ]; then # move the output of previous analysis, if any.
    MODIFICATION_TIMESTAMP=$(stat $ANALYSIS_FILE | grep Modify:)
    MODIFICATION_TIMESTAMP=${MODIFICATION_TIMESTAMP#"Modify: "}
    mv $ANALYSIS_FILE "${ANALYSIS_FILE#".txt"} - $MODIFICATION_TIMESTAMP.txt"
fi

CATEGORIES=(
"boost"
"bugprone"
"cppcoreguidelines"
"clang-analyzer"
"modernize"
"mpi"
"openmp"
"performance"
"portability"
"readability"
)

OPTION_CATEGORIES="-*"
for category in "${CATEGORIES[@]}"; do
    OPTION_CATEGORIES=$OPTION_CATEGORIES,$category-*
done

# -I/usr/include/eigen3/ \
OPTION_INCLUDE_DIRS="
-I/usr/lib64/mpi/gcc/openmpi2/include \
-Imbsolve-lib/include \
-Imbsolve-tool/include \
-Isolver-cpu/include \
-Isolver-generic/include \
-Isolver-interface/include \
-Iwriter-hdf5/include \
"

for i in $(find -name '*.[cht]pp'); do
    case $i in
        ./build*) # ignore files in the build folder
            continue
        ;;
    esac

    clang-tidy --checks=$OPTION_CATEGORIES \
    $i -- $OPTION_INCLUDE_DIRS -DUSE_MPI=1 \
    | tee -a $ANALYSIS_FILE
done

echo "------------------------------"
echo "Result of clang-tidy analysis:"
count=$(cat $ANALYSIS_FILE | grep -Poc "\^")
echo "Total number of errors and warnings: $count"
if [[ $count -eq 0 ]]; then
    exit 0
done

echo "Number of errors and warnings by category:"
for category in "${CATEGORIES[@]}"; do
    count=$(cat $ANALYSIS_FILE | grep -Poc "\[$category-")
    echo "  $category: $count"
done