cd unit-test
if [ ! -d "build" ]; then
    ./ci-scripts/build-test.sh || {
        echo "Could not build unit tests."
        exit 1;
    }
fi

./build/unit-test
