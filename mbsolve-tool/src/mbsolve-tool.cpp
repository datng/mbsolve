/*
 * mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
 *
 * Copyright (c) 2016, Computational Photonics Group, Technical University of
 * Munich.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

/**
 * \defgroup MBSOLVE_TOOL mbsolve-tool
 * Runs different simulation setups.
 */
#if !defined(USE_MPI)
# error "You should specify USE_MPI=0 or USE_MPI=1 on the compile line"
#endif

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <boost/program_options.hpp>

#include <mbsolve.hpp>
#include <cw_solver.hpp>


#if USE_MPI
#include <mpi.h>
#include <cw_solver_factory_mpi.hpp>
#else
#include <cw_solver_factory.hpp>
#include <boost/timer/timer.hpp>
#endif

// TODO: make omp optional
#include <omp.h>


namespace po = boost::program_options;

static std::string device_file;
static std::string output_file;
static std::string scenario_file;
static std::string solver_method;
static std::string writer_method;
static unsigned int num_lvl = 0;
static mbsolve::real sim_endtime;
static unsigned int num_gridpoints;
static std::string init_method;
#if USE_MPI
#else
static boost::timer::cpu_timer timer;
#endif

/**
 * Parses the given command line arguments.
 * For a complete list of the available arguments call the program with -h.
 *
 * \param [in] argc Number of input arguments.
 * \param [in] argv Pointer to the argument values.
 */

static void parse_args(int argc, char **argv)
{
    po::options_description general("Allowed options");
    general.add_options()
	("help,h", "Print usage")
	("device,d", po::value<std::string>(&device_file)->required(),
	 "Set device settings file")
	("output,o", po::value<std::string>(&output_file), "Set output file")
	("scenario,s", po::value<std::string>(&scenario_file),
	 "Set scenario settings file")
	("method,m", po::value<std::string>(&solver_method)->required(),
	 "Set solver method")
	("writer,w", po::value<std::string>(&writer_method)->required(),
	 "Set writer")
    ("endtime,e", po::value<mbsolve::real>(&sim_endtime),
     "Set simulation end time")
    ("gridpoints,g", po::value<unsigned int>(&num_gridpoints),
     "Set number of spatial grid points")
    ("levels,l", po::value<unsigned int>(&num_lvl),
     "Set number of levels")
    ("initialization,i", po::value<std::string>(&init_method),
    "Set initialization of the electric field array");

    po::variables_map vm;
    try {
	    po::store(po::parse_command_line(argc, argv, general), vm);
    	if (vm.count("help")) {
    	    std::cout << general;
    	    exit(0);
    	}
    	po::notify(vm);
    } catch (po::error& e) {
 	    std::cerr << "Error: " << e.what() << std::endl;
	    std::cerr << general;
	    exit(1);
    }

    if (vm.count("device")) {
	    device_file = vm["device"].as<std::string>();
    }
    if (vm.count("scenario")) {
	    scenario_file = vm["scenario"].as<std::string>();
    }
}

/**
 * Returns the current time in seconds.
 * Depending on the usage of MPI or not, the method uses MPI_Wtime() or
 * boost::timer.
 *
 * \return The current time in seconds.
 */
static double elapsed_time(){
#if USE_MPI
    return MPI_Wtime();
#else
    boost::timer::cpu_times times = timer.elapsed();
    return times.wall * 1e-9;
#endif
}

/**
 * mbsolve-tool main function.
 * \ingroup MBSOLVE_TOOL
 *
 * Specify the simulation setup using the -d parameter (The available setups
 * are described below) and the solver method using the -m parameter.
 * For the complete list of parameters, run mbsolve-tool -h.
 */
int main(int argc, char **argv)
{
    int num_total_cores = 1;
    int num_threads = 1;
    int mpi_size = 1;
    int mpi_rank = 0;

#if USE_MPI

#ifdef _OPENMP
    int thread_support;

    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &thread_support);
    if (thread_support==MPI_THREAD_SINGLE)
    {
        std::cout << "The MPI implementation has no support for threading" <<
            std::endl;
        MPI_Finalize();
        exit(1);
    }
    num_threads = omp_get_max_threads();
#else
    MPI_Init(&argc, &argv);
#endif // _OPENMP
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
#endif // USE_MPI
    num_total_cores = num_threads * mpi_size;

    /* parse command line arguments */
    parse_args(argc, argv);

    try {
        std::shared_ptr<mbsolve::device> dev;
        std::shared_ptr<mbsolve::scenario> scen;

        if (device_file == "song2005") {
            /**
             * The song2005 setup features a three-level active region which
             * is excited by a sech pulse. For details see literature:
             * Song X. et al., Propagation of a Few-Cycle Laser Pulse
             * in a V-Type Three-Level System, Optics and Spectroscopy,
             * Vol. 99, No. 4, 2005, pp. 517–521
             * https://doi.org/10.1134/1.2113361
             */

            /* set up quantum mechanical description */
            std::vector<mbsolve::real> energies = {
                0,
                2.3717 * mbsolve::HBAR * 1e15,
                2.4165 * mbsolve::HBAR * 1e15
            };

            mbsolve::qm_operator H(energies);

            std::vector<mbsolve::complex> dipoles = {
                mbsolve::E0 * 9.2374e-11,
                mbsolve::E0 * 9.2374e-11 * sqrt(2),
                0
            };

            mbsolve::qm_operator u({0, 0, 0}, dipoles);

            mbsolve::real rate = 1e10;
            std::vector<std::vector<mbsolve::real> > scattering_rates = {
                { 0, rate, rate },
                { rate, 0, rate },
                { rate, rate, 0 } };

            auto relax_sop = std::make_shared<mbsolve::qm_lindblad_relaxation>
                (scattering_rates);

            auto qm = std::make_shared<mbsolve::qm_description>
                (6e24, H, u, relax_sop);

            auto mat_vac = std::make_shared<mbsolve::material>("Vacuum");
            mbsolve::material::add_to_library(mat_vac);
            auto mat_ar = std::make_shared<mbsolve::material>("AR_Song", qm);
            mbsolve::material::add_to_library(mat_ar);

            dev = std::make_shared<mbsolve::device>("Song");
            dev->add_region(std::make_shared<mbsolve::region>
                            ("Active region", mat_ar, 0, 150e-6));

            /* default settings */
            if (num_gridpoints == 0) {
                num_gridpoints = 32768;
            }
            if (sim_endtime < 1e-21) {
                sim_endtime = 80e-15;
            }

            mbsolve::qm_operator rho_init({1, 0, 0});

            /* Song basic scenario */
            scen = std::make_shared<mbsolve::scenario>
                ("Basic", num_gridpoints, sim_endtime, rho_init);

            auto sech_pulse = std::make_shared<mbsolve::sech_pulse>
                ("sech", 0.0, mbsolve::source::hard_source, 3.5471e9,
                 3.8118e14, 17.248, 1.76/5e-15, -M_PI/2);
            scen->add_source(sech_pulse);

            scen->add_record(std::make_shared<mbsolve::record>
                             ("d11", mbsolve::record::type::density, 1, 1,
                              0, 0));
            scen->add_record(std::make_shared<mbsolve::record>
                             ("d22", mbsolve::record::type::density, 2, 2,
                              0, 0));
            scen->add_record(std::make_shared<mbsolve::record>
                             ("d33", mbsolve::record::type::density, 3, 3,
                              0, 0));
            scen->add_record(std::make_shared<mbsolve::record>("e", 0, 0.0));


        } else if (device_file == "ziolkowski1995") {
            /**
             * The ziolkowski1995 setup is a self induced transparency (SIT)
             * setup that consists of a two-level active region embedded in
             * two vacuum section. For details see literature:
             * https://doi.org/10.1103/PhysRevA.52.3082
             */

            /* set up quantum mechanical description */
            auto qm = std::make_shared<mbsolve::qm_desc_2lvl>
                (1e24, 2 * M_PI * 2e14, 6.24e-11, 1.0e10, 1.0e10);

            /* materials */
            auto mat_vac = std::make_shared<mbsolve::material>("Vacuum");
            auto mat_ar = std::make_shared<mbsolve::material>
                ("AR_Ziolkowski", qm);
            mbsolve::material::add_to_library(mat_vac);
            mbsolve::material::add_to_library(mat_ar);

            /* set up device */
            dev = std::make_shared<mbsolve::device>("Ziolkowski");
            dev->add_region(std::make_shared<mbsolve::region>
                            ("Vacuum left", mat_vac, 0, 7.5e-6));
            dev->add_region(std::make_shared<mbsolve::region>
                            ("Active region", mat_ar, 7.5e-6, 142.5e-6));
            dev->add_region(std::make_shared<mbsolve::region>
                            ("Vacuum right", mat_vac, 142.5e-6, 150e-6));

            /* default settings */
            if (num_gridpoints == 0) {
                num_gridpoints = 32768;
            }
            if (sim_endtime < 1e-21) {
                sim_endtime = 200e-15;
            }

            mbsolve::qm_operator rho_init({1, 0, 0});

            /* Ziolkowski basic scenario */
            scen = std::make_shared<mbsolve::scenario>
                ("Basic", num_gridpoints, sim_endtime, rho_init);

            auto sech_pulse = std::make_shared<mbsolve::sech_pulse>
                //("sech", 0.0, mbsolve::source::hard_source, 4.2186e9/2, 2e14,
                ("sech", 0.0, mbsolve::source::hard_source, 4.2186e9, 2e14,
                 10, 2e14);
            scen->add_source(sech_pulse);

            scen->add_record(std::make_shared<mbsolve::record>
                             ("inv12", 2.5e-15));
            scen->add_record(std::make_shared<mbsolve::record>("e", 2.5e-15));


        } else if (device_file == "tzenov2018-cpml") {
            /**
             * The tzenov2018-cpml setup consists of an absorber region
             * embedded in two gain regions. Each region is modeled as a
             * two-level system. The results show that short pulses are
             * generated due to colliding pulse mode-locking (CPML).
             * For details see literature:
             * https://doi.org/10.1088/1367-2630/aac12a
             */

            /* set up quantum mechanical descriptions */
            auto qm_gain = std::make_shared<mbsolve::qm_desc_2lvl>
                (5e21, 2 * M_PI * 3.4e12, 2e-9, 1.0/10e-12, 1.0/200e-15, 1.0);

            auto qm_absorber = std::make_shared<mbsolve::qm_desc_2lvl>
                (1e21, 2 * M_PI * 3.4e12, 6e-9, 1.0/3e-12, 1.0/160e-15);

            /* materials */
            auto mat_absorber = std::make_shared<mbsolve::material>
                ("Absorber", qm_absorber, 12.96, 1.0, 500);
            auto mat_gain = std::make_shared<mbsolve::material>
                ("Gain", qm_gain, 12.96, 1.0, 500);
            mbsolve::material::add_to_library(mat_absorber);
            mbsolve::material::add_to_library(mat_gain);

            /* set up device */
            dev = std::make_shared<mbsolve::device>("tzenov-cpml");
            dev->add_region(std::make_shared<mbsolve::region>
                            ("Gain R", mat_gain, 0, 0.5e-3));
            dev->add_region(std::make_shared<mbsolve::region>
                            ("Absorber", mat_absorber, 0.5e-3, 0.625e-3));
            dev->add_region(std::make_shared<mbsolve::region>
                            ("Gain L", mat_gain, 0.625e-3, 1.125e-3));

            /* default settings */
            if (num_gridpoints == 0) {
                num_gridpoints = 8192;
            }
            if (sim_endtime < 1e-21) {
                sim_endtime = 2e-9;
            }

            mbsolve::qm_operator rho_init({0.5, 0.5}, {0.001});

            /* basic scenario */
            scen = std::make_shared<mbsolve::scenario>
                ("basic", num_gridpoints, sim_endtime, rho_init);

            scen->add_record(std::make_shared<mbsolve::record>
                             ("inv12", 1e-12));
            scen->add_record(std::make_shared<mbsolve::record>("e", 1e-12));
            scen->add_record(std::make_shared<mbsolve::record>
                             ("e0", mbsolve::record::electric, 1, 1, 0.0,
                              0.0));
            scen->add_record(std::make_shared<mbsolve::record>
                             ("h0", mbsolve::record::magnetic, 1, 1, 0.0,
                              1.373e-7));

        } else if (device_file == "marskar2011") {
            /**
             * The marskar2011 setup is a 6-level anharmonic ladder system.
             * In the scenario, the interaction with a few-cycle Gaussian pulse
             * is simulated. For details see literature:
             * https://doi.org/10.1364/OE.19.016784
             */

            /* set up quantum mechanical description */
            std::vector<mbsolve::real> energies(6, 0.0);
            for (int i = 1; i < 6; i++) {
                energies[i] = energies[i - 1] + (1.0 - 0.1 * (i - 3)) *
                    mbsolve::HBAR * 2 * M_PI * 1e13;
            }

            mbsolve::qm_operator H(energies);

            mbsolve::real dipole = 1e-29;
            std::vector<mbsolve::complex> dipoles = {
                dipole,
                0, dipole,
                0, 0, dipole,
                0, 0, 0, dipole,
                0, 0, 0, 0, dipole
            };

            mbsolve::qm_operator u({0, 0, 0, 0, 0, 0}, dipoles);

            mbsolve::real rate = 1e12;
            std::vector<std::vector<mbsolve::real> > scattering_rates = {
                { rate, 1.0/(1.0e-12), 0, 0, 0, 0 },
                { 3.82950e+11, rate, 1.0/(1.1e-12), 0, 0, 0 },
                { 0, 3.77127e+11, rate, 1.0/(1.2e-12), 0, 0 },
                { 0, 0, 3.74488e+11, rate, 1.0/(1.3e-12), 0 },
                { 0, 0, 0, 3.74467e+11, rate, 1.0/(1.4e-12) },
                { 0, 0, 0, 0, 3.76675e+11, rate }
            };

            auto relax_sop = std::make_shared<mbsolve::qm_lindblad_relaxation>
                (scattering_rates);

            auto qm = std::make_shared<mbsolve::qm_description>
                (1e25, H, u, relax_sop);

            /* materials */
            auto mat_vac = std::make_shared<mbsolve::material>("Vacuum");
            mbsolve::material::add_to_library(mat_vac);
            auto mat_ar = std::make_shared<mbsolve::material>("AR_Marsk", qm);
            mbsolve::material::add_to_library(mat_ar);

            /* set up device */
            dev = std::make_shared<mbsolve::device>("Marskar");
            dev->add_region(std::make_shared<mbsolve::region>
                            ("Active region", mat_ar, 0, 1e-3));

            /* default settings */
            if (num_gridpoints == 0) {
                num_gridpoints = 8192;
            }
            if (sim_endtime < 1e-21) {
                sim_endtime = 2e-12;
            }

            mbsolve::qm_operator rho_init({0.60, 0.23, 0.096, 0.044, 0.02,
                        0.01});

            /* Marskar basic scenario */
            scen = std::make_shared<mbsolve::scenario>
                ("Basic", num_gridpoints, sim_endtime, rho_init);

            /* input pulse */
            mbsolve::real tau = 100e-15;
            auto pulse = std::make_shared<mbsolve::gaussian_pulse>
                ("gaussian",
                 0.0, /* position */
                 mbsolve::source::hard_source,
                 5e8, /* amplitude */
                 1e13, /* frequency */
                 3.0 * tau, /* phase: 3*tau */
                 tau /* tau */
                 );
            scen->add_source(pulse);

            /* select data to be recorded */
            mbsolve::real sample_time = 0.0;
            mbsolve::real sample_pos = 0.0;
            scen->add_record(std::make_shared<mbsolve::record>
                             ("d11", mbsolve::record::type::density, 1, 1,
                              sample_time, sample_pos));
            scen->add_record(std::make_shared<mbsolve::record>
                             ("d22", mbsolve::record::type::density, 2, 2,
                              sample_time, sample_pos));
            scen->add_record(std::make_shared<mbsolve::record>
                             ("d33", mbsolve::record::type::density, 3, 3,
                              sample_time, sample_pos));
            scen->add_record(std::make_shared<mbsolve::record>
                             ("d44", mbsolve::record::type::density, 4, 4,
                              sample_time, sample_pos));
            scen->add_record(std::make_shared<mbsolve::record>
                             ("d55", mbsolve::record::type::density, 5, 5,
                              sample_time, sample_pos));
            scen->add_record(std::make_shared<mbsolve::record>
                             ("d66", mbsolve::record::type::density, 6, 6,
                              sample_time, sample_pos));
            scen->add_record(std::make_shared<mbsolve::record>
                             ("e", mbsolve::record::type::electric, 0, 0,
                              sample_time, sample_pos));

        } else if (device_file == "marskar2011-nlvl") {
            /**
             * The marskar2011-nlvl setup is a generalized n-level anharmonic
             * ladder system.
             * In the scenario, the interaction with a few-cycle Gaussian
             * pulse is simulated. For details see literature:
             * https://doi.org/10.1364/OE.19.016784
             */
            if(num_lvl <= 1){
                throw std::invalid_argument(std::string("Command line option ")
                    + std::string("'-l' is missing or has an invalid value."));
            }
            /* set up quantum mechanical description */
            std::vector<mbsolve::real> energies(num_lvl, 0.0);
            for (int i = 1; i < num_lvl; i++) {
                energies[i] = energies[i - 1] + (1.0 - 0.1 * (i - 3)) *
                    mbsolve::HBAR * 2 * M_PI * 1e13;
            }

            mbsolve::qm_operator H(energies);

            mbsolve::real dipole = 1e-29;
            std::vector<mbsolve::complex> dipoles = {dipole};
            for(int i = 1; i < num_lvl - 1; i++){
                for(int j = 0; j < i; j++){
                    dipoles.push_back(0);
                }
                dipoles.push_back(dipole);
            }

            std::vector<mbsolve::real> u1 = {0};
            for(int i = 1; i < num_lvl; i++){
                u1.push_back(0);
            }
            mbsolve::qm_operator u(u1, dipoles);

            mbsolve::real rate = 1e12;
            mbsolve::real temperature = 600;
            std::vector<std::vector<mbsolve::real> > scattering_rates;
            for(int i = 0; i < num_lvl; i++){
                std::vector<mbsolve::real> v;
                for(int j = 0; j < num_lvl; j++){
                    if(i == j){
                        v.push_back(rate);
                    }
                    else if(i == j - 1){
                        v.push_back(1.0/((1 + 0.1 * i) * 1e-12));
                    }
                    else if(i == j + 1){
                        v.push_back((1.0/((1 + 0.1 * j) * 1e-12)) *
                            exp(-(energies[i] - energies[j])/
                                (mbsolve::KB * temperature)));
                    }
                    else{
                        v.push_back(0);
                    }
                }
                scattering_rates.push_back(v);
            }

            auto relax_sop = std::make_shared<mbsolve::qm_lindblad_relaxation>
                (scattering_rates);

            auto qm = std::make_shared<mbsolve::qm_description>
                (1e25, H, u, relax_sop);

            /* materials */
            auto mat_vac = std::make_shared<mbsolve::material>("Vacuum");
            mbsolve::material::add_to_library(mat_vac);
            auto mat_ar = std::make_shared<mbsolve::material>("AR_Marsk", qm);
            mbsolve::material::add_to_library(mat_ar);

             /* set up device */
            dev = std::make_shared<mbsolve::device>("Marskar");
            dev->add_region(std::make_shared<mbsolve::region>
                            ("Active region", mat_ar, 0, 1e-3));

            /* default settings */
            if (num_gridpoints == 0) {
                num_gridpoints = 8192;
            }
            if (sim_endtime < 1e-21) {
                sim_endtime = 2e-12;
            }

            /* solve for initial density matrix values */
            /* fractions is a vector storing the fraction of each entry pair */
            std::vector<mbsolve::real> fractions;
            for(int i = 1; i < num_lvl; i++){
                fractions.push_back(exp(-(energies[i] - energies[i-1])/
                    (mbsolve::KB * temperature)));
            }

            mbsolve::real prod = 0;
            for(int i = 0; i < num_lvl; i++){
                mbsolve::real inc = 1;
                for(int j = 0; j < i; j++){
                    inc *= fractions[j];
                }
                prod += inc;
            }

            std::vector<mbsolve::real> rho;
            rho.push_back(1/prod);
            for(int i = 0; i < num_lvl - 1; i++){
                rho.push_back(fractions[i] * rho[i]);
            }
            mbsolve::qm_operator rho_init(rho);

            /* Marskar basic scenario */
            scen = std::make_shared<mbsolve::scenario>
                ("Basic", num_gridpoints, sim_endtime, rho_init);

            /* input pulse */
            mbsolve::real tau = 100e-15;
            auto pulse = std::make_shared<mbsolve::gaussian_pulse>
                ("gaussian",
                 0.0, /* position */
                 mbsolve::source::hard_source,
                 5e8, /* amplitude */
                 1e13, /* frequency */
                 3.0 * tau, /* phase: 3*tau */
                 tau /* tau */
                 );
            scen->add_source(pulse);

            /* select data to be recorded */
            mbsolve::real sample_time = 0.0;
            mbsolve::real sample_pos = 0.0;
            scen->add_record(std::make_shared<mbsolve::record>
                             ("e", mbsolve::record::type::electric, 0, 0,
                              sample_time, sample_pos));
            for(int i = 1; i <= num_lvl; i++){
                scen->add_record(std::make_shared<mbsolve::record>
                             ("d" + std::to_string(i) + std::to_string(i),
                              mbsolve::record::type::density, i, i,
                              sample_time, sample_pos));
            }

        } else if (device_file == "paris6mm") {
            /**
             * The paris6mm setup is a 6 mm cavity QCL which is modeled
             * with an absorber region embedded in two gain regions.
             * Each region is modeled as a three-level system.
             */

            /* only gain region, no absorber */
            bool sim_plain = false;

            /* set up Hamiltonian and dipole moment operator */
            std::vector<mbsolve::real> energies = {
                0, 0, -2 * M_PI * mbsolve::HBAR * 2.475e12
            };
            mbsolve::qm_operator H(energies);

            std::vector<mbsolve::complex> dipoles = {
                0, 0, mbsolve::E0 * 4.4e-9
            };
            mbsolve::qm_operator u({0, 0, 0}, dipoles);

            /* corresponds to lower laser level life time */
            mbsolve::real gamma_13 = 1.0/5e-12;
            /* corresponds to superlattice transport time */
            mbsolve::real gamma_21 = 1.0/60e-12;
            /* corresponds to upper laser level life time */
            mbsolve::real gamma_32 = 1.0/8e-12;
            /* dephasing time */
            mbsolve::real gamma_pd = 1.0/800e-15;
            mbsolve::real gamma_22 = (gamma_pd - gamma_13 - gamma_32);
            /* scattering rates gain region */
            std::vector<std::vector<mbsolve::real> > rates_gain = {
                {      0.0,      0.0, gamma_13 },
                { gamma_21, gamma_22,      0.0 },
                {      0.0, gamma_32,      0.0 }
            };
            auto relax_sop_gain =
                std::make_shared<mbsolve::qm_lindblad_relaxation>(rates_gain);

            /* scattering rates absorber region */
            gamma_32 = 1.0/5e-12;
            gamma_22 = (gamma_pd - gamma_13 - gamma_32);
            std::vector<std::vector<mbsolve::real> > rates_abs = {
                {      0.0,      0.0, gamma_13 },
                { gamma_21, gamma_22,      0.0 },
                {      0.0, gamma_32,      0.0 }
            };
            auto relax_sop_abs =
                std::make_shared<mbsolve::qm_lindblad_relaxation>(rates_abs);

            /* quantum mechanical descriptions */
            mbsolve::real N = 2e22 * 19.8/70.9;
            auto qm_absorber = std::make_shared<mbsolve::qm_description>
                (N, H, u, relax_sop_abs);
            auto qm_gain = std::make_shared<mbsolve::qm_description>
                (N, H, u, relax_sop_gain);

            /* materials */
            auto mat_absorber = std::make_shared<mbsolve::material>
                ("Absorber", qm_absorber, 12.96, 1.0, 750);
            auto mat_gain = std::make_shared<mbsolve::material>
                ("Gain", qm_gain, 12.96, 1.0, 750);
            mbsolve::material::add_to_library(mat_absorber);
            mbsolve::material::add_to_library(mat_gain);

            /* set up device */
            mbsolve::real length_dev = 6.00e-3;
            mbsolve::real length_abs = 0.75e-3;
            mbsolve::real pos_abs = length_dev/2;
            mbsolve::real abs_start = pos_abs - length_abs/2;
            mbsolve::real abs_end = pos_abs + length_abs/2;
            if (sim_plain) {
                dev = std::make_shared<mbsolve::device>("paris6mm-plain");
                dev->add_region(std::make_shared<mbsolve::region>
                                ("Gain R", mat_gain, 0, length_dev));
            } else {
                dev = std::make_shared<mbsolve::device>("paris6mm");
                dev->add_region(std::make_shared<mbsolve::region>
                                ("Gain R", mat_gain, 0, abs_start));
                dev->add_region(std::make_shared<mbsolve::region>
                                ("Abs", mat_absorber, abs_start, abs_end));
                dev->add_region(std::make_shared<mbsolve::region>
                                ("Gain L", mat_gain, abs_end, length_dev));
            }
            /* default settings */
            if (num_gridpoints == 0) {
                num_gridpoints = 8192;
            }
            if (sim_endtime < 1e-21) {
                sim_endtime = 2e-9;
            }

            mbsolve::qm_operator rho_init({0.8219, 0.1096, 0.0685},
                                          {0.0, 0.0, 0.001});

            /* basic scenario */
            scen = std::make_shared<mbsolve::scenario>
                ("basic", num_gridpoints, sim_endtime, rho_init);

            scen->add_record(std::make_shared<mbsolve::record>
                             ("e0", mbsolve::record::electric, 0, 0, 0.0,
                              length_dev));
            /* TODO: remove the following records - only for debugging! */
            scen->add_record(std::make_shared<mbsolve::record>
                             ("inv12", 1e-11));
            scen->add_record(std::make_shared<mbsolve::record>("e", 1e-11));

        } else {
            throw std::invalid_argument("Specified device not found!");
        }

        if(mpi_rank == 0){
            std::cout << "Using device file " << device_file << std::endl;
            std::cout << "Number of gridpoints: " << num_gridpoints << std::endl;
        }

        /* tic */
        double start_time = elapsed_time();

        std::shared_ptr<mbsolve::cw_solver> solver =
            mbsolve::cw_solver_factory::create_solver(solver_method, dev,
                                                        scen, init_method);

        double setup_time;
        if(mpi_rank == 0){
            /* toc */
            setup_time = elapsed_time();
            std::cout << "Total number of cores: " << num_total_cores << std::endl;
            std::cout << "Time required (setup): " << (setup_time - start_time)
                      << std::endl;

            std::cout << solver->get_name() << std::endl;
        }

        /* execute solver */
        solver->run();

        if(mpi_rank == 0){
            /* toc */
            double run_time = elapsed_time();
            std::cout << "Time required (run): " << (run_time - setup_time) << std::endl;

            /* grid point updates per second */
            double gpups = 1e-6 / (run_time - setup_time) * scen->get_num_gridpoints() *
                scen->get_endtime()/scen->get_timestep_size();
            std::cout << "Performance: " << gpups << " MGPU/s" << std::endl;
            std::cout << "#Iterations: " <<
                 scen->get_num_timesteps() << std::endl;

            /* write results */
            mbsolve::writer writer(writer_method);
            writer.write(output_file, solver->get_results(), dev, scen);

            /* toc */
            double write_time = elapsed_time();
            std::cout << "Time required (write): " << (write_time - run_time)
                      << std::endl;

            std::cout << "Time required (total): " << (write_time - start_time) << std::endl;
        }

    } catch (std::exception& e) {
        std::cout << "Error: " << e.what() << std::endl;
        exit(1);
    }
#if USE_MPI
    MPI_Finalize();
#endif

    exit(0);
}
