% cumpute_diff    Computes difference of two hdf5 output files.

% mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
%
% Copyright (c) 2019, Computational Photonics Group, Technical University 
% of Munich.
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software Foundation,
% Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

clear all;
close all;

%  choose 1st hdf5 file
[filename_1, folder_1] = uigetfile('../../*.hdf', 'Select 1st result data');
f_1 = fullfile(folder_1, filename_1);
if (filename_1 == 0)
   return;
end

%  choose 2nd hdf5 file
[filename_2, folder_2] = uigetfile('../../*.hdf', 'Select 2nd result data');
f_2 = fullfile(folder_2, filename_2);
if (filename_2 == 0)
   return;
end

% read global attributes
d_x = h5readatt(f_1, '/', 'gridpoint_size');
d_t = h5readatt(f_1, '/', 'timestep_size');
t_e = h5readatt(f_1, '/', 'sim_endtime');
L_x = h5readatt(f_1, '/', 'dev_length');

% grid
x = 0:d_x:L_x;

% data
e_1 = h5read(f_1, '/e/real');
e_2 = h5read(f_2, '/e/real');

if size(e_1, 1) ~= size(e_2, 1) || ...
   size(e_1, 2) ~= size(e_2, 2) 
    fprintf('Can not compare data with different sizes\n');
    return;
end

format long;
err = sum(sum(e_2 .^ 2 - e_1 .^ 2));
fprintf('Squared error: %f\n', err);