#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" marskar2011.py: Runs Marskar2011 six-level setup."""

# mbsolve: Framework for solving the Maxwell-Bloch/-Lioville equations
#
# Copyright (c) 2016, Computational Photonics Group, Technical University
# of Munich.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

import sys
sys.path.append('./mbsolve-lib/')
sys.path.append('./solver-cpu/')
sys.path.append('./solver-generic/')
sys.path.append('./writer-hdf5/')
sys.path.append('./solver-interface/')

import numpy as np
import math

from mpi4py import MPI

import pymbsolvelib as mb
import pysolvercpu
import pysolvergeneric
import pywriterhdf5
import pysolverinterface as intf

# setup quantum mechanical description
energies = np.zeros(6)
for i in range(1, 6):
  energies[i] = energies[i - 1] + (1.0 - 0.1 * (i - 3)) * mb.HBAR * 2 * np.pi * 1e13

H = mb.qm_operator(energies)

dipole = 1e-29

dipoles = [	dipole,
		0, dipole,
		0, 0, dipole,
		0, 0, 0, dipole,
		0, 0, 0, 0, dipole]

u = mb.qm_operator([0, 0, 0, 0, 0, 0], dipoles)

rate = 1e12

scattering_rates = [[rate, 1.0/(1.0e-12), 0, 0, 0, 0],
		[ 3.82950e+11, rate, 1.0/(1.1e-12), 0, 0, 0 ],
                [ 0, 3.77127e+11, rate, 1.0/(1.2e-12), 0, 0 ],
                [ 0, 0, 3.74488e+11, rate, 1.0/(1.3e-12), 0 ],
                [ 0, 0, 0, 3.74467e+11, rate, 1.0/(1.4e-12) ],
                [ 0, 0, 0, 0, 3.76675e+11, rate ]]

relax_sop = mb.qm_lindblad_relaxation(scattering_rates)

relax_sop = mb.qm_lindblad_relaxation(scattering_rates)

qm = mb.qm_description(1e25, H, u, relax_sop)

# materials
# Marskar active region material
mat_ar = mb.material("AR_Marsk", qm)
mb.material.add_to_library(mat_ar)

# Marskar setup
dev = mb.device("Marskar")
dev.add_region(mb.region("Active region", mat_ar, 0, 1e-3))

# initial density matrix
rho_init = mb.qm_operator([ 0.60, 0.23, 0.096, 0.044, 0.02, 0.01 ])

# scenario
gridpoints = 8192
sim_time = 200e-15
sce = mb.scenario("Basic", gridpoints, sim_time, rho_init)

# select data to be recorded
sample_time = 0.0
sample_pos = 0.0
sce.add_record(mb.record("d11", mb.record.density, 1, 1, sample_time, sample_pos))
sce.add_record(mb.record("d22", mb.record.density, 2, 2, sample_time, sample_pos))
sce.add_record(mb.record("d33", mb.record.density, 3, 3, sample_time, sample_pos))
sce.add_record(mb.record("d44", mb.record.density, 4, 4, sample_time, sample_pos))
sce.add_record(mb.record("d55", mb.record.density, 5, 5, sample_time, sample_pos))
sce.add_record(mb.record("d66", mb.record.density, 6, 6, sample_time, sample_pos))

# add source
tau = 100e-15
pulse = mb.gaussian_pulse("gaussian", 0.0, mb.source.hard_source, 5e8, 1e13, 3.0 * tau, tau)
sce.add_source(pulse)

# run solver
sol = intf.cw_solver_factory.create_mpi_solver("cpu-fdtd-red-6lvl-cvr-rodr", dev, sce, MPI.COMM_WORLD)
if(MPI.COMM_WORLD.Get_rank() == 0):
  print('Solver ' + sol.get_name()  + ' started')
tic = MPI.Wtime()
sol.run()
toc = MPI.Wtime()
if(MPI.COMM_WORLD.Get_rank() == 0):
  print('Solver ' + sol.get_name() + ' finished in ' + str(toc - tic) + ' sec')

# write results
wri = mb.writer("hdf5")
outfile = dev.get_name() + "_" + sce.get_name() + "." + wri.get_extension()
if(MPI.COMM_WORLD.Get_rank() == 0):
  results = sol.get_results()
  wri.write(outfile, results, dev, sce)
  print('Solver ' + sol.get_name() + ' wrote to disc.')

