#!/bin/bash
#SBATCH -o /home/hpc/pr27ke/ge34cay5/runs/hybrid_scaling_10lvl_32nodes.%j.%N.out
#SBATCH -D /home/hpc/pr27ke/ge34cay5/test_build
#SBATCH -J mbsolve
#SBATCH --get-user-env
#SBATCH --clusters=mpp2
#SBATCH --ntasks=32
#SBATCH --cpus-per-task=28
#SBATCH --export=NONE
#SBATCH --time=02:00:00

source /etc/profile.d/modules.sh
module unload mpi.intel
module load hdf5/1.8.20-cxx
module load mpi.intel/2017_gcc
module load boost/1.68.0

export OMP_PLACES=cores
export OMP_PROC_BIND=close

cd $HOME/test_build

all_nodes=(1 2 4 8 16 32)
iterations=3

export OMP_NUM_THREADS=1

# baseline
# reproducibility
for it in `seq 1 $iterations`; do

        mpiexec -n 1 ./mbsolve-tool/mbsolve-tool -d marskar2011-nlvl -m cpu-fdtd-red-10lvl-cvr-rodr -w hdf5 -e 1e-15 -l 10

done

export OMP_NUM_THREADS=28

# vary process count
for nodes in ${all_nodes[@]}; do

        let time_base=30
        let endtime=$nodes*time_base

        # reproducibility
        for it in `seq 1 $iterations`; do

                mpiexec -n $nodes -ppn 1 ./mbsolve-tool/mbsolve-tool -d marskar2011-nlvl -m cpu-fdtd-red-10lvl-cvr-rodr -w hdf5 -e `echo $endtime`e-15 -l 10

        done

done

