#!/bin/bash
#SBATCH -o /home/hpc/pr27ke/ge34cay5/runs/hybrid_scaling_10lvl_2nodes.%j.%N.out
#SBATCH -D /home/hpc/pr27ke/ge34cay5/test_build
#SBATCH -J mbsolve
#SBATCH --get-user-env
#SBATCH --clusters=mpp2
#SBATCH --ntasks=2
#SBATCH --cpus-per-task=28
#SBATCH --export=NONE
#SBATCH --time=02:00:00

source /etc/profile.d/modules.sh
module unload mpi.intel
module load hdf5/1.8.20-cxx
module load mpi.intel/2017_gcc
module load boost/1.68.0

export OMP_PLACES=cores
export OMP_PROC_BIND=close

cd $HOME/test_build

process_start=1
process_end=28
iterations=3

export OMP_NUM_THREADS=1

# baseline
# reproducibility
for it in `seq 1 $iterations`; do

        mpiexec -n 1 ./mbsolve-tool/mbsolve-tool -d marskar2011-nlvl -m cpu-fdtd-red-10lvl-cvr-rodr -w hdf5 -e 1e-15

done

# vary process count
for processes in `seq $process_start $process_end`; do

        let time_base=2
        let endtime=$processes*$time_base

        export OMP_NUM_THREADS=$processes


        # reproducibility
        for it in `seq 1 $iterations`; do

                mpiexec -n 2 -ppn 1 ./mbsolve-tool/mbsolve-tool -d marskar2011-nlvl -m cpu-fdtd-red-10lvl-cvr-rodr -w hdf5 -e `echo $endtime`e-15

        done

done
