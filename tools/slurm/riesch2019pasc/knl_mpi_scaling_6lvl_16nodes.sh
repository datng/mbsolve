#!/bin/bash
#SBATCH -o /home/hpc/pr27ke/ge34cay5/runs/mpi_scaling_6lvl_16nodes.%j.%N.out
#SBATCH -D /home/hpc/pr27ke/ge34cay5/knl_build
#SBATCH -J mbsolve
#SBATCH --get-user-env
#SBATCH --clusters=mpp3
#SBATCH --nodes=16
#SBATCH --export=NONE
#SBATCH --time=02:00:00

source /etc/profile.d/modules.sh
module unload mpi.intel
module load hdf5/1.8.20-cxx
module load mpi.intel/2019
module load boost/1.68.0

export OMP_PLACES=cores
export OMP_PROC_BIND=close

cd $HOME/knl_build

all_nodes=(1 2 4 8 16)
iterations=3

export OMP_NUM_THREADS=1

# baseline
# reproducibility
for it in `seq 1 $iterations`; do
        mpiexec -n 1 ./mbsolve-tool/mbsolve-tool -d marskar2011 -m hybrid-fdtd-red-6lvl-cvr-rodr -w hdf5 -e 5e-15
done

# vary process count
for nodes in ${all_nodes[@]}; do

        let time_base=300
        let endtime=$nodes*$time_base
        let procs=64*$nodes

        # reproducibility
        for it in `seq 1 $iterations`; do

                I_MPI_PIN=1 I_MPI_PIN_PROCESSOR_LIST=allcores:shift=2 mpiexec -n $procs -ppn 64 ./mbsolve-tool/mbsolve-tool -d marskar2011 -m hybrid-fdtd-red-6lvl-cvr-rodr -w hdf5 -e `echo $endtime`e-15

        done

done