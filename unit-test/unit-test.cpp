#include "mbsolve-lib.test/device.test.hpp"
#include "mbsolve-lib.test/material.test.hpp"

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
