cmake_minimum_required(VERSION 3.6)
project(unit-test)

find_package(Eigen3 REQUIRED)
include_directories(${EIGEN3_INCLUDE_DIR})
include_directories(unit-test
    include
    ../mbsolve-lib/include
    ../external/googletest/googletest
    ../external/googletest/googletest/include
    ../external/googletest/googlemock
    ../external/googletest/googlemock/include
)

set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")
set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} -lgcov")

file(GLOB_RECURSE ALL_TESTS *.test/*.test.hpp)

file(GLOB_RECURSE ALL_mbsolve-lib ../mbsolve-lib/*.[cht]pp)

add_executable(unit-test
    ${ALL_TESTS}
    ${ALL_mbsolve-lib}
    unit-test.cpp
    ../external/googletest/googletest/src/gtest-all.cc
    ../external/googletest/googlemock/src/gmock-all.cc
)

set_property(TARGET unit-test PROPERTY CXX_STANDARD 11)

target_link_libraries(unit-test pthread)
