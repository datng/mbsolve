#ifndef MBSOLVE_LIB_MATERIAL_TEST_HPP
#define MBSOLVE_LIB_MATERIAL_TEST_HPP
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <material.hpp>

TEST(material, constructor)
{
    mbsolve::material m("sample-material");
    ASSERT_EQ(m.get_id(), "sample-material");
    ASSERT_EQ(m.get_qm(), nullptr);
    ASSERT_EQ(m.get_rel_permeability(), 1.0);
    ASSERT_EQ(m.get_rel_permittivity(), 1.0);
    ASSERT_EQ(m.get_losses(), 0.0);
    ASSERT_EQ(m.get_overlap_factor(), 1.0);
}

TEST(material, duplication)
{
    mbsolve::material m1("mat1");
    mbsolve::material::add_to_library(m1);
    mbsolve::material m2("mat1");
    mbsolve::material::add_to_library(m2);
}
/*
TEST(material, library)
{
    mbsolve::material m1("mat1");
    mbsolve::material::add_to_library(m1);
    ASSERT_EQ(*mbsolve::material::get_from_library("mat1"), m1);
    ASSERT_EQ(mbsolve::material::get_from_library("mat2"), nullptr);
    mbsolve::material m2("mat2");
    mbsolve::material::add_to_library(m2);
    ASSERT_EQ(*mbsolve::material::get_from_library("mat2"), m2);
}*/
#endif // MBSOLVE_LIB_MATERIAL_TEST_HPP
