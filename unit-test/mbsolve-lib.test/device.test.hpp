#ifndef MBSOLVE_LIB_DEVICE_TEST_HPP
#define MBSOLVE_LIB_DEVICE_TEST_HPP
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <device.hpp>

TEST(device, constructor)
{
    mbsolve::device d("sample-device");

    ASSERT_EQ(d.get_length(), 0.0);
    ASSERT_EQ(d.get_minimum_permittivity(), 1e42);
    ASSERT_EQ(d.get_name(), "sample-device");
    ASSERT_EQ(d.get_regions().size(), 0);
    ASSERT_EQ(d.get_used_materials().size(), 0);
}
#endif // MBSOLVE_LIB_DEVICE_TEST_HPP
